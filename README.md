# Physicum #


Physicum is latin for physical as in physical activity and will be the basis
for a personal need I have to keep track of only simple things.

Such as nutrition, meals, workouts with reps and what to work out.

## Running ##
"development" is the local development profile where physicumc-client is
fetching resources from the disk, and not from the classpath JAR inside the War.
This enables rapid development and changes of the frontend without having to
wait for recompiles.

Run local development mode with:
: ./gradlew clean tomcatRunWar -Dspring.profiles.active="development"

If you want to run it with resources in a compiled JAR such as it would
be in a production environment, use the profile "production".

Run local production mode with:
: ./gradlew clean tomcatRunWar -Dspring.profiles.active="production"

Debuging
========
Debugging the application takes some extra work, as we need to set up a
remote entry point to attach our debugger to.

When starting the Gradle script now, it will wait for IntelliJ to connect
to the debugger before actually running the application.

# Linux #
In a console, issue this command to start the application with debugging up:
./gradlew clean tomcatRunWar -Dspring.profiles.active="development" --no-daemon -Dorg.gradle.debug=true 

Or run the provided script 'physicumRunDev.sh'.

In IntelliJ go to Run options -> Remote and add a new remote debugg option.
The defaults are fine. Keep them.

# Windows #

This will be added in the future.

Deploying
=========
The application will deploy to Tomcat with ease.

The only step necessary is to add a file named 'setenv.sh' to the /bin
directory of tomcat and add: 
    JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=production"

This will make sure that the application switch to the production profile.
This is useful, as it means the environment where its being deployed is
responsible for deciding which mode to run in. It also guarantees that the
application never starts up in development on a production server.

Note that production primarily is the only build supported for deploy to
a standalone Tomcat container.

# Roadmap #
The roadmap is yet only in my head. I may look into moving the application
to Spring Boot in the future, and will try to incorporate Event Sourcing at some point.