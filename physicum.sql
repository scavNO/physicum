-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: physicum
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authority`
--

DROP TABLE IF EXISTS `authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority` (
  `authority_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authority`
--

LOCK TABLES `authority` WRITE;
/*!40000 ALTER TABLE `authority` DISABLE KEYS */;
INSERT INTO `authority` VALUES (1,'ROLE_ADMIN','Admin'),(2,'ROLE_USER','User'),(3,'ROLE_NEW','New');
/*!40000 ALTER TABLE `authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercise`
--

DROP TABLE IF EXISTS `exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercise` (
  `exercise_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`exercise_id`),
  UNIQUE KEY `exercise_id_UNIQUE` (`exercise_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercise`
--

LOCK TABLES `exercise` WRITE;
/*!40000 ALTER TABLE `exercise` DISABLE KEYS */;
INSERT INTO `exercise` VALUES (4,'Barbell Row'),(2,'Barbell Shrugs'),(1,'Benchpress'),(3,'Triceps Extensions');
/*!40000 ALTER TABLE `exercise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercise_muscle`
--

DROP TABLE IF EXISTS `exercise_muscle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercise_muscle` (
  `exercise_id` bigint(20) NOT NULL,
  `muscle_id` bigint(20) NOT NULL,
  PRIMARY KEY (`exercise_id`,`muscle_id`),
  KEY `fk_exercise_has_muscle_muscle1_idx` (`muscle_id`),
  KEY `fk_exercise_has_muscle_exercise1_idx` (`exercise_id`),
  CONSTRAINT `fk_exercise_has_muscle_exercise1` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`exercise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_exercise_has_muscle_muscle1` FOREIGN KEY (`muscle_id`) REFERENCES `muscle` (`muscle_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercise_muscle`
--

LOCK TABLES `exercise_muscle` WRITE;
/*!40000 ALTER TABLE `exercise_muscle` DISABLE KEYS */;
INSERT INTO `exercise_muscle` VALUES (1,1),(4,2),(1,3),(3,3),(2,4);
/*!40000 ALTER TABLE `exercise_muscle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `ingredient_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `energy` int(3) DEFAULT NULL,
  `protein` int(3) DEFAULT NULL,
  `fat_total` int(3) DEFAULT NULL,
  `fat_saturated` int(3) DEFAULT NULL,
  `carbohydrate` int(3) DEFAULT NULL,
  `carbohydrate_sugar` int(3) DEFAULT NULL,
  `sodium` int(3) DEFAULT NULL,
  PRIMARY KEY (`ingredient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (1,'Kjøttdeig',800,22,18,15,0,0,1),(2,'Pannekaker',1200,18,5,3,30,15,2),(3,'Brød',200,6,0,0,70,25,5),(9,'Potet',400,5,1,0,25,2,2),(10,'Drittmat',0,0,0,0,0,0,200),(11,'Kjøttpølse',450,0,0,0,0,0,20);
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal`
--

DROP TABLE IF EXISTS `meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal` (
  `meal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `note` varchar(45) DEFAULT NULL,
  `date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY (`meal_id`,`user_id`,`category_id`),
  UNIQUE KEY `meal_id` (`meal_id`),
  KEY `fk_meal_user1_idx` (`user_id`),
  KEY `fk_meal_meal_category1_idx` (`category_id`),
  CONSTRAINT `fk_meal_meal_category1` FOREIGN KEY (`category_id`) REFERENCES `meal_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_meal_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal`
--

LOCK TABLES `meal` WRITE;
/*!40000 ALTER TABLE `meal` DISABLE KEYS */;
INSERT INTO `meal` VALUES (9,'Eggerøre','2014-05-13 08:00:00',2,1),(51,'Test #1','2014-07-20 00:00:00',1,0),(52,'Test #2','2014-07-21 00:00:00',1,4);
/*!40000 ALTER TABLE `meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_category`
--

DROP TABLE IF EXISTS `meal_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal_category` (
  `category_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `unique_category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_category`
--

LOCK TABLES `meal_category` WRITE;
/*!40000 ALTER TABLE `meal_category` DISABLE KEYS */;
INSERT INTO `meal_category` VALUES (0,'Breakfast #1'),(1,'Breakfast #2'),(2,'Lunch'),(3,'Snack'),(4,'Dinner'),(5,'Supper'),(6,'Pre Workout'),(7,'Post Workout');
/*!40000 ALTER TABLE `meal_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_ingredient`
--

DROP TABLE IF EXISTS `meal_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal_ingredient` (
  `meal_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  PRIMARY KEY (`meal_id`,`ingredient_id`),
  KEY `fk_meal_has_ingredient_ingredient1_idx` (`ingredient_id`),
  KEY `fk_meal_has_ingredient_meal1_idx` (`meal_id`),
  CONSTRAINT `fk_meal_has_ingredient_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_meal_has_ingredient_meal1` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`meal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_ingredient`
--

LOCK TABLES `meal_ingredient` WRITE;
/*!40000 ALTER TABLE `meal_ingredient` DISABLE KEYS */;
INSERT INTO `meal_ingredient` VALUES (52,1),(51,2),(51,9),(52,9);
/*!40000 ALTER TABLE `meal_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `muscle`
--

DROP TABLE IF EXISTS `muscle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muscle` (
  `muscle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`muscle_id`),
  UNIQUE KEY `muscle_id_UNIQUE` (`muscle_id`),
  UNIQUE KEY `muscle_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muscle`
--

LOCK TABLES `muscle` WRITE;
/*!40000 ALTER TABLE `muscle` DISABLE KEYS */;
INSERT INTO `muscle` VALUES (2,'Biceps'),(1,'Chest'),(4,'Traps'),(3,'Triceps');
/*!40000 ALTER TABLE `muscle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(256) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `profilePicture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','Johny Admin','$2a$10$mcO45dqFXDOJ61ucv0ChUuMJrzssacVkhOFBv5HaFCS6NcyeZUmOu','admin@itops.no',NULL),(2,'test','Frank Test','$2a$10$QGeFVBjDyt23QqkVuCWpwu1Xyavt.HQIjUz7pUJi8OCrNgqPoQbN.','test@test.no',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_authority`
--

DROP TABLE IF EXISTS `user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_authority` (
  `created` datetime DEFAULT NULL,
  `authority_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`authority_id`,`user_id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_authority_authority` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`authority_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_authority_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_authority`
--

LOCK TABLES `user_authority` WRITE;
/*!40000 ALTER TABLE `user_authority` DISABLE KEYS */;
INSERT INTO `user_authority` VALUES ('2013-05-06 21:04:36',1,1),('2013-05-06 21:04:36',2,1),('2014-02-14 00:00:00',2,2);
/*!40000 ALTER TABLE `user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout`
--

DROP TABLE IF EXISTS `workout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout` (
  `workout_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `note_before` varchar(200) DEFAULT NULL,
  `note_after` varchar(200) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY (`workout_id`,`user_id`,`category_id`),
  UNIQUE KEY `workout_id` (`workout_id`),
  KEY `fk_workout_user1_idx` (`user_id`),
  KEY `fk_workout_workout_category1_idx` (`category_id`),
  CONSTRAINT `fk_workout_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_workout_workout_category1` FOREIGN KEY (`category_id`) REFERENCES `workout_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout`
--

LOCK TABLES `workout` WRITE;
/*!40000 ALTER TABLE `workout` DISABLE KEYS */;
INSERT INTO `workout` VALUES (6,'Test Workout #1','2014-07-28 15:00:00','Test workout before','Test workout after',1,2),(7,'Test Workout #2','2014-07-30 10:00:00','Test before #222222','Test after #222222',1,2);
/*!40000 ALTER TABLE `workout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout_category`
--

DROP TABLE IF EXISTS `workout_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id_UNIQUE` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout_category`
--

LOCK TABLES `workout_category` WRITE;
/*!40000 ALTER TABLE `workout_category` DISABLE KEYS */;
INSERT INTO `workout_category` VALUES (1,'Cardio','Cardio'),(2,'Weight Training','Weight Training');
/*!40000 ALTER TABLE `workout_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout_exercise`
--

DROP TABLE IF EXISTS `workout_exercise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout_exercise` (
  `workout_id` bigint(20) NOT NULL,
  `exercise_id` bigint(20) NOT NULL,
  `sets` int(3) DEFAULT NULL,
  `reps` int(3) DEFAULT NULL,
  PRIMARY KEY (`workout_id`,`exercise_id`),
  KEY `fk_workout_has_exercise_exercise1_idx` (`exercise_id`),
  KEY `fk_workout_has_exercise_workout1_idx` (`workout_id`),
  CONSTRAINT `fk_workout_has_exercise_exercise1` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`exercise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_workout_has_exercise_workout1` FOREIGN KEY (`workout_id`) REFERENCES `workout` (`workout_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout_exercise`
--

LOCK TABLES `workout_exercise` WRITE;
/*!40000 ALTER TABLE `workout_exercise` DISABLE KEYS */;
INSERT INTO `workout_exercise` VALUES (6,1,4,8),(6,2,3,8),(7,3,4,8),(7,4,3,8);
/*!40000 ALTER TABLE `workout_exercise` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-03  0:35:46
