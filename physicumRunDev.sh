#!/bin/bash

#This is the Linux run script for local development of Physicum.
#It will export the correct Gradle options to the current active shell and
#set up a debuggable JVM session for the Gradle wrapped application instance
#running the development profile.

echo 'Setting environment variables for Gradle'
export GRADLE_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"

echo 'Starting Physicum with profile DEVELOPMENT, NO DAEMON and DEBUG TRUE'
echo 'Waiting for remote debugger to attach...'
./gradlew clean tomcatRunWar -Dspring.profiles.active="development" --no-daemon -Dorg.gradle.debug=true
