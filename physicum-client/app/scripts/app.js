'use strict';

var PhysicumApp = angular.module('PhysicumApp',
        ['ngRoute',
        'PhysicumApp.filters',
        'PhysicumApp.services',
        'PhysicumApp.directives',
        'ui.bootstrap']);

PhysicumApp.config(['$routeProvider', function ($routeProvider) {

    /**
     * Route for the meal view.
     */
    $routeProvider.when('/meal', {
        templateUrl: 'views/meal/layout.html',
        controller: MealController
    });

    /**
     * Route for the mealLog view.
     */
    $routeProvider.when('/meal/log', {
        templateUrl: 'views/meal/log/layout.html',
        controller: MealLogController
    });

    /**
     * Route for the workout view.
     */
    $routeProvider.when('/workout', {
        templateUrl: 'views/workout/layout.html',
        controller: WorkoutController
    });

    /**
     * Route for the workoutLog view.
     */
    $routeProvider.when('/workout/log', {
        templateUrl: 'views/workout/log/layout.html',
        controller: WorkoutLogController
    });


    /**
     * Route for the profile view.
     */
    $routeProvider.when('/profile', {
        templateUrl: 'views/profile/layout.html',
        controller: ProfileController
    });

    /**
     * Route for the ingredient view
     */
    $routeProvider.when('/ingredient', {
        templateUrl: 'views/ingredient/layout.html',
        controller: IngredientController
    });

    /**
     * If there is no route, redirect to
     * the users profile as a natural stating point
     */
    $routeProvider.otherwise({
        redirectTo: '/profile'
    });
}]);