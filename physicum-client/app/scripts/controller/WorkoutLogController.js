'use strict';

var WorkoutLogController = function($scope, $http) {

    $scope.getWorkouts = function() {
        $http.get('workout/all').success(function(workouts){
            $scope.workouts = workouts;
        });
    };

    $scope.getWorkouts();
};