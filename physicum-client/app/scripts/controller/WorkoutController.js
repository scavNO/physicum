'use strict';

var WorkoutController = function($scope, $http, $log) {

    $scope.workout = {};
    $scope.workoutExercises = [];
    $scope.workoutExerciseList = [];

    /**
     * Create the initial Workout object.
     */
    $scope.addNewWorkout = function() {
        $http.post('workout/add', angular.toJson($scope.workout))
            .success(function(workout) {
                $scope.workout.id = workout.id;
                $scope.add();
            })
            .error(function() {
                $log.error("Unable to add workout!");
                $log.error(angular.toJson($scope.workout));
            });
    };

    /**
     * Dirty method to create the WorkoutExercise json object
     * to send it to the server after a workout has been registered.
     */
    $scope.add = function() {
        angular.forEach($scope.workoutExercises, function(data) {
            var workoutExercise = {};
            workoutExercise.reps = data.reps;
            workoutExercise.sets = data.sets;
            workoutExercise.weight = data.weight;
            workoutExercise.workout = $scope.workout;
            delete data.reps;
            delete data.sets;
            delete data.weight;
            workoutExercise.exercise = data;
            $scope.workoutExerciseList.push(workoutExercise);
        });

        $http.post('workout/addAll', angular.toJson($scope.workoutExerciseList))
            .success(function(workout) {
                //Do something, we just added a workout.
            })
            .error(function() {
                $log.error("Unable to add workout-exercise!");
            });
    };

    $scope.getCategories = function() {
        $http.get('workout/category/all').success(function(workoutCategories){
            $scope.workoutCategories = workoutCategories;
        });
    };

    $scope.findExercise = function(query) {
        return $http.get('workout/exercise/find/'+query).then(function(res) {
            var result = [];
            angular.forEach(res.data, function(exercise){
                result.push(exercise);
            });
            return result;
        });
    };

    /**
     * Relieve one exercise added from the exercise search field.
     * This is only a vanilla exercise object with no extra information.
     * @param exercise
     */
    $scope.addExercise = function(exercise) {
        $scope.workoutExercises.push(exercise);
    };

    $scope.getCategories();
};