'use strict';

var IngredientController = function($scope, $http, $modal, $log) {

    $scope.ingredient = {};

    $scope.fetchIngredientList = function() {
        $http.get('ingredient/all').success(function(ingredientList) {
            $scope.ingredients = ingredientList;
        })
    };

    $scope.addIngredient = function() {
        $log.info(angular.toJson($scope.ingredient));
        $http.post('ingredient/add', angular.toJson($scope.ingredient))
            .success(function() {
                $scope.fetchIngredientList();
            }).error(function () {
                $log.error("Unable to fucking post -> " + angular.toJson($scope.ingredient));
            });
    };

    $scope.fetchIngredientList();

};