'use strict';

var ProfileController = function($scope, $http) {

    $scope.fetchUser = function() {
        $http.get('user').success(function(user){
            $scope.user = user;
        });
    };

    $scope.fetchLatestMeals = function() {
        $http.get('meal/all/0/4').success(function(meals){
            $scope.latestMeal = meals;
        });
    };

    $scope.fetchUser();
    $scope.fetchLatestMeals();
};