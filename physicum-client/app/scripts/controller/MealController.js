'use strict';

var MealController = function($scope, $http, $modal, $log) {

    $scope.meal = {};
    $scope.meal.ingredients = [];
    $scope.selectedIngredient = {};
    $scope.selectedIngredients = [];


    $scope.addNewMeal = function() {
        $log.info(angular.toJson($scope.meal));
        $http.post('meal/add', angular.toJson($scope.meal))
            .success(function() {
            }).error(function () {
                $log.error("Unable to post -> " + angular.toJson($scope.meal));
            });
    };

    $scope.removeIngredient = function(ingredient, index) {
        $scope.meal.ingredients.splice(index, 1);
    };

    $scope.removeAllIngredients = function() {
        for(var field in $scope.meal) {
            $scope.meal[field] = '';
        }

        while($scope.selectedIngredients.length > 0) {
            $scope.ingredients.push(
                $scope.selectedIngredients.pop()
            );
        }
    };

    $scope.getCategories = function() {
        $http.get('meal/category/all').success(function(mealCategories){
            $scope.mealCategories = mealCategories;
        });
    };

    $scope.getCategories();

    /**
     * Searches through the backend for any hits from
     * the input box.
     * @param val
     * @returns {*}
     */
    $scope.findIngredient = function(val) {
        return $http.get('ingredient/find/'+val).then(function(res) {
            var result = [];
            angular.forEach(res.data, function(ingredient){
                result.push(ingredient);
            });
            return result;
        });
    };

    /**
     * This callback adds the selected $item
     * to the meals ingredients array on select.
     * @param ingredient
     */
    $scope.selectIngredient = function(ingredient) {
        $scope.meal.ingredients.push(ingredient);
    };


    /**
     * DatePicker START.
     */
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    /**
     * DatePicker END
     */

    /**
     * TimePicker START
     */
    $scope.initDate = new Date('2016-15-20');
    $scope.format = 'dd-MMMM-yyyy HH:mm';

    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.ismeridian = true;
    $scope.toggleMode = function() {
        $scope.ismeridian = ! $scope.ismeridian;
    };

    $scope.update = function() {
        var d = new Date();
        d.setHours(14);
        d.setMinutes(0);
        $scope.mytime = d;
    };

    $scope.changed = function () {
        $log.info('Time changed to: ' + $scope.mytime);
    };

    $scope.clear = function() {
        $scope.mytime = null;
    };

    /**
     * DatePicker END
     */
};