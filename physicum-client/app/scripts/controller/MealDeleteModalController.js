'use strict';

var MealDeleteModalController = function ($scope, $http, $modalInstance, $log, meal) {

    $scope.meal = meal;

    $scope.mealDeleteModalClose = function () {
        $modalInstance.close(meal);
    };

    $scope.mealDeleteModalDelete = function() {
        $log.info("Preparing to delete");
        $http.delete('meal/delete/'+$scope.meal.id)
            .success(function(deletedMeal) {
                $modalInstance.close(deletedMeal);
            });
    };
};
