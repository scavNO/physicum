'use strict';

var MealLogController = function($scope, $http, $modal, $log) {

    $scope.meal = {};

    /**
     * Variables for pagination of meal list.
     */
    $scope.maxSize = 3; //Just an arbitrary number to simulate a count() of the total meals.
    $scope.totalMeals = 175;
    $scope.currentPage = 1;

    $scope.fetchPageMealList = function() {
        $http.get('meal/all/'+($scope.currentPage-1)+'/4').success(function(mealList){
            $scope.meals = mealList;
        });
    };

    /**
     * Set the current page for the pageable meals.
     * @param pageNo
     */
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.fetchMeal = function(meal) {
            $scope.meal = meal;
    };

    $scope.removeMeal = function(meal) {
        $http.delete('meal/delete/' + meal.id).success(function() {
            $scope.currentPage = 1;
            $scope.fetchPageMealList();
        });
    };

    $scope.mealDeleteModal = function (meal) {
        var modalInstance = $modal.open({
            templateUrl: 'views/meal/log/modal_delete.html',
            controller: MealDeleteModalController,
            size: 'sm',
            resolve: {
                meal: function () {
                    return meal;
                }
            }
        });

        modalInstance.result.then(function () {
        }, function () {
//            $log.info("HELLO WAT");
            $scope.fetchPageMealList();
        });
    };

    $scope.fetchPageMealList();

};