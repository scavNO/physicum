physicum-client
---------------

This module is the physicum-client which is the web frontend for the Physicum
application. It contains everything that happens within the browser and is
expanding upon tools like npm, bower, gulp and gradle (to manage gulp builds).

The location of scripts and styles are structured in a way that they are reachable
both in development and production, by the servlet running the application in context.

Resources
---------
It has two different resources depending on the profile the application is runnig in.

1.  ./gradlew clean tomcatRunWar -Dspring.profiles.active="development"
    Development will use the raw /src directory to server content directly from disk
    while running the embedded Tomcat server. This is to provide means for instant
    reflection of changes made to physicum-client while the backend runs. It should
    make quick iterations during development easier.

2. ./gradlew clean tomcatRunWar -Dspring.profiles.active="production"
    Production will use the grails build chain to issue commands to npm to build
    the entire client package and send the output to /dist where it will be
    picked up by the gradle jar module and compiled to the classpath.
    This means that the compiled output from /dist no longer is editable.
    
Working with the module
-----------------------
Dependencies:
If you do not have these, the gradle build will fail.
 - node.js and npm(bundled) (http://nodejs.org/)
 - development dependencies (npm install)
 - gulp (npm install gulp)
 - bower (npm install bower)

To add or update npm packages, add them to package.json and issue "npm install"
To update bower dependencies, add the dependency to bower.json and issue "bower install"
To build to /dist: "npm run build"

Structure
---------
There are 5 primary folders in this module:

1.      /lib -> bower resource directory
2.      /scripts -> scripts that are written for this application
3.      /styles -> styles that are not in bower / custom .css files
4.      /view -> all html files in the application
5.      /img -> static image resource, must not be renamed to "images" as this will
        crash with the context of the image servlet running within the application.

    