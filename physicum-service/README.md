# Physicum Service

A quick note of how auth.html and index.html is being rendered using Thymeleaf.

As Gulp removes the / from <link> tags, it will not be considered valid in the eyes
of Thymeleaf, even though it is perfectly valid. To avoid this issue Thymeleaf is
currently running LEGACYHTML5 while waiting for Thymeleafe 3.

The reason Thymeleaf renders this wrong, has to do with how it uses SAX to parse the
HTML as XML, and therefore, void elements are not handled correctly.

What is this
============
This is the server application for Physicum. It controls pretty much everything from
database access, security, services, REST API's and two initial views (index.html and auth.html).

The architecture follows a mixture of both Domain Driven development and Service oriented development.
This means that there are a service layer built on top of a repository layer. The service layer is the
only valid entry point into the application, and for now its being accessed only by controllers.