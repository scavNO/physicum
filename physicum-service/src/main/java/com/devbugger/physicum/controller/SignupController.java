package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.UserSignup;
import com.devbugger.physicum.exception.FieldValidationException;
import com.devbugger.physicum.exception.ResourceNotFoundException;
import com.devbugger.physicum.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping(value = "/signup")
public class SignupController {

    private static final Logger logger = LoggerFactory.getLogger(SignupController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public User signUp(ModelMap model,
                        @RequestBody UserSignup userSignup,
                        BindingResult bindingResult,
                        Locale locale) {

        if(bindingResult.hasErrors()) {
            //Well well well...
            throw new ResourceNotFoundException((long) 5);
        }
        if(!userSignup.getEmail().equals(userSignup.getEmailCheck())) {
            throw new FieldValidationException("Email does not match.");
        }

        model.clear();

        return userService.save(userSignup.getUser());
    }
}
