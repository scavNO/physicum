package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.meal.MealCategory;
import com.devbugger.physicum.service.MealCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/meal/category")
public class MealCategoryController {

    @Autowired
    private MealCategoryService mealCategoryService;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<MealCategory> getAll() {
        return mealCategoryService.findAll();
    }
}
