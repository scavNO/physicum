package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.service.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/workout/exercise")
public class ExerciseController {

    @Autowired
    private ExerciseService exerciseService;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Exercise> all() {
        return exerciseService.findAll();
    }

    @RequestMapping(value = "find/{name}", method = RequestMethod.GET)
    public List<Exercise> findByName(@PathVariable("name") String name) {
        return exerciseService.findByName(name);
    }
}
