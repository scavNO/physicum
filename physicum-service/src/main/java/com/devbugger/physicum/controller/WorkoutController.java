package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.editors.ExerciseEditor;
import com.devbugger.physicum.domain.editors.WorkoutCategoryEditior;
import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutCategory;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.devbugger.physicum.exception.ResourceNotFoundException;
import com.devbugger.physicum.repository.WorkoutRepository;
import com.devbugger.physicum.security.SecurityContext;
import com.devbugger.physicum.service.ExerciseService;
import com.devbugger.physicum.service.WorkoutExerciseService;
import com.devbugger.physicum.service.WorkoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping(value = "/workout")
public class WorkoutController {

    private static final Logger logger = LoggerFactory.getLogger(WorkoutController.class);

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private WorkoutExerciseService workoutExerciseService;

    @InitBinder
    protected void initBinder(HttpServletRequest httpServletRequest, ServletRequestDataBinder binder) {
        binder.registerCustomEditor(WorkoutCategory.class, new WorkoutCategoryEditior(this.workoutService));
        binder.registerCustomEditor(Exercise.class, new ExerciseEditor(this.exerciseService));
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Workout> all() {
        return workoutService.findAll(
                SecurityContext.getUserDetails().getUser());
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Workout add(ModelMap model,
                    @RequestBody Workout workout,
                    BindingResult bindingResult,
                    Locale locale) {

        if(bindingResult.hasErrors()) {
            logger.error("Unable to save workout!");
//            throw new ResourceNotFoundException((long) 5);
        }

        workout.setUser(SecurityContext.getUserDetails().getUser());

        logger.info("Tying to save...");
        logger.info(workout.toString());

        model.clear();

        return workoutService.save(workout);
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        Workout workout = workoutService.findById(id);
        if(workout != null) {
            workoutService.delete(workout);
        }
    }

    @RequestMapping(value = "addAll", method = RequestMethod.POST)
    public List<WorkoutExercise> addAll(ModelMap model,
                                        @RequestBody List<WorkoutExercise> workoutExerciseList,
                                        BindingResult bindingResult,
                                        Locale locale) {

        if(bindingResult.hasErrors()) {
            logger.error("Unable to save workout!");
//            throw new ResourceNotFoundException((long) 5);
        }


        logger.info("Tying to save...");
        workoutExerciseList.forEach((workoutExercise) ->
                logger.info(workoutExercise.toString()));

        return workoutExerciseService.saveAll(workoutExerciseList);

    }
}