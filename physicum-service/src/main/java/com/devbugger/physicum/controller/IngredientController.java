package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.editors.IngredientEditor;
import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/ingredient")
public class IngredientController {

    @Autowired
    private MealService mealService;

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    protected void initBinder(HttpServletRequest httpServletRequest, ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Ingredient.class, new IngredientEditor(this.mealService));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Ingredient get(@PathVariable("id") Long id) {
        return mealService.ingredientFindById(id);
    }

    @RequestMapping(value = "find/{name}", method = RequestMethod.GET)
    public List<Ingredient> findByName(@PathVariable("name") String name) {
        return mealService.ingredientsFindByName(name);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Ingredient> getAllMealsByUser() {
        return mealService.ingredientFindAll();
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Ingredient add(ModelMap model,
                      @RequestBody Ingredient ingredient,
                      BindingResult bindingResult,
                      Locale locale) {

        if(bindingResult.hasErrors()) {
            //Return an error message in case of binding errors.
            return new Ingredient();
        }

        model.clear();
        return mealService.ingredientSave(ingredient);
    }
}
