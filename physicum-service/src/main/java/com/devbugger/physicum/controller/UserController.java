package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.security.SecurityContext;
import com.devbugger.physicum.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public User get() {
        return userService.findById(SecurityContext.getUserDetails().getUser().getId());
    }


    /**
     *  Removed because it is currently not necessary
     *  to find specific users by their user id.
     */
//    @RequestMapping(value = "{id}", method = RequestMethod.GET)
//    public User getUser(@PathVariable("id") Long id) {
//        return userService.findById(id);
//    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<User> getUsers() {
        return userService.findAll();
    }

}