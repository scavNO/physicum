package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.editors.IngredientEditor;
import com.devbugger.physicum.domain.editors.MealCategoryEditor;
import com.devbugger.physicum.domain.editors.MealEditor;
import com.devbugger.physicum.domain.editors.UserEditor;
import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.domain.meal.MealCategory;
import com.devbugger.physicum.security.SecurityContext;
import com.devbugger.physicum.service.MealCategoryService;
import com.devbugger.physicum.service.MealService;
import com.devbugger.physicum.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@RestController
@RequestMapping("/meal")
public class MealController {

    private static final Logger logger = LoggerFactory.getLogger(MealController.class);

    @Autowired
    private MealService mealService;

    @Autowired
    private UserService userService;

    @Autowired
    private MealCategoryService mealCategoryService;

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    protected void initBinder(HttpServletRequest httpServletRequest, ServletRequestDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor(this.userService));
        binder.registerCustomEditor(Meal.class, new MealEditor(this.mealService));
        binder.registerCustomEditor(Ingredient.class, new IngredientEditor(this.mealService));
        binder.registerCustomEditor(MealCategory.class, new MealCategoryEditor(this.mealCategoryService));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Meal get(@PathVariable("id") Long id) {
        return mealService.findById(id);
    }

    @RequestMapping(value = "all/{page}/{limit}", method = RequestMethod.GET)
    public List<Meal> latest(@PathVariable("page") int page, @PathVariable("limit") int limit) {
        User user = SecurityContext.getUserDetails().getUser();
        return mealService.pageableMealList(user, page, limit);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Meal add(ModelMap model,
                     @RequestBody Meal meal,
                     BindingResult bindingResult,
                     Locale locale) {

        logger.info("Trying to add object: \n" + meal.toString());

        if(bindingResult.hasErrors()) {
            return new Meal();
        }

        meal.setUser(SecurityContext.getUserDetails().getUser());
        model.clear();

        return mealService.save(meal);
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        Meal meal = mealService.findById(id);
        if(meal != null) {
            mealService.delete(meal);
        }
    }
}