package com.devbugger.physicum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthController {
	
	@Autowired
	private MessageSource messageSource;

	@RequestMapping()
	public String logIn() {
		return "auth/auth";
	}

	@RequestMapping("/logout")
	public String logOut() {
//		model.addAttribute("message", new Message("alert alert-success",
//				messageSource.getMessage("message_create_task_success",
//				new Object[]{},
//				locale)));

//        model.addAttribute("buildInfo", getBuildInfo());

		return "/auth/auth";
	}

	@RequestMapping("/fail")
	public String loginFail() {
		
//		model.addAttribute("message", new Message("alert alert-error",
//				messageSource.getMessage("message_login_failed",
//				new Object[]{},
//				locale)));

//        model.addAttribute("buildInfo", getBuildInfo());
		
		return "/auth/auth";
	}

    /**
     * Simple method for fetching maven properties
     * for build information.
     * @TODO: Make this method return JSON so it can be requested anywhere.
     * @return String
     */
//    private String getBuildInfo() {
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        InputStream is = loader.getResourceAsStream("buildInfo.properties");
//        Properties properties = new Properties();
//        try {
//            properties.load(is);
//        } catch (IOException e) {
//            //Unable to fetch buildInfo.properites...
//        }
//
//        return properties.getProperty("artifactId") + "-" + properties.getProperty("version");
//    }
}