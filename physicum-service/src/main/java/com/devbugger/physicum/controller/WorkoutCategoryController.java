package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.workout.WorkoutCategory;
import com.devbugger.physicum.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/workout/category")
public class WorkoutCategoryController {

    @Autowired
    private WorkoutService workoutService;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<WorkoutCategory> all() {
        return workoutService.findAllCategies();
    }
}
