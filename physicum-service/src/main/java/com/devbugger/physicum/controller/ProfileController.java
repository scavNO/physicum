package com.devbugger.physicum.controller;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.security.SecurityContext;
import com.devbugger.physicum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public User getCurrentUser() {
        return userService.findById(SecurityContext.getUserDetails().getUser().getId());
    }
}