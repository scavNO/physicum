package com.devbugger.physicum.controller;


import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.devbugger.physicum.service.WorkoutExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/workout/exercises/")
public class WorkoutExerciseController {

    @Autowired
    private WorkoutExerciseService workoutExerciseService;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<WorkoutExercise> all() {
        return workoutExerciseService.findAll();
    }
}
