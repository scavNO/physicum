package com.devbugger.physicum.controller.system;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.security.SecurityContext;
import com.devbugger.physicum.security.ValidAuthority;
import com.devbugger.physicum.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This controller redirects to different HTTP Status
 * errors to provide custom views.
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 2/13/14
 */
@Controller
@RequestMapping("/system/")
public class HttpErrorController {

    @Autowired
    private UserService userService;

    /**
     * .
     * Handles occurrences of HTTP Status Code 403 - FORBIDDEN
     * handling two very basic forms of 403 use cases.
     *
     * 1. Not belonging to a family
     * 2. Not having access (duh).
     * @param model
     * @return
     */
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String get403(Model model) {
        User user = userService.findById(SecurityContext.getUserDetails().getUser().getId());
        boolean newUser = false;

        for(Authority authority : user.getAuthorities()) {
            if(authority.getAuthority().equals(ValidAuthority.ROLE_NEW.toString())) {
                newUser = true;
            }
        }

        model.addAttribute("newUser", newUser);
        model.addAttribute("user", user);

        return "system/403";
    }
}
