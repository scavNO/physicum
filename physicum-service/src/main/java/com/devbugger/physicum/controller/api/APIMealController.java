package com.devbugger.physicum.controller.api;

import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/meal")
public class APIMealController {

    @Autowired
    private MealService mealService;

    @RequestMapping(value = "ingredients/all", method = RequestMethod.GET)
    public List<Ingredient> getAll() {
        return mealService.ingredientFindAll();
    }
}
