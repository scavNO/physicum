package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.service.AuthorityService;

import java.beans.PropertyEditorSupport;

public class AuthorityEditor extends PropertyEditorSupport {

    private final AuthorityService authorityService;

    public AuthorityEditor(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Authority authority =  authorityService.findById(Long.parseLong(text));
        setValue(authority);
    }
}
