package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.service.MealService;

import java.beans.PropertyEditorSupport;

public class MealEditor extends PropertyEditorSupport {

    private final MealService mealService;

    public MealEditor(MealService mealService) {
        this.mealService = mealService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Meal meal =  mealService.findById(Long.parseLong(text));
        setValue(meal);
    }
}