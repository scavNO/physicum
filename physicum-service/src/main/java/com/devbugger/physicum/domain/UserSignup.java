package com.devbugger.physicum.domain;

/**
 * Used when a new user signs up.
 */
public class UserSignup {

    private String username;

    private String password;

    private String name;

    private String email;

    private String emailCheck;

    private User user;

    public UserSignup() {

    }

    public UserSignup(String username, String password, String name, String email, String emailCheck) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.emailCheck = emailCheck;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailCheck() {
        return emailCheck;
    }

    public void setEmailCheck(String emailCheck) {
        this.emailCheck = emailCheck;
    }

    public User getUser() {
        User user = new User();
        user.setUsername(username);
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        return user;
    }

    @Override
    public String toString() {
        return "UserSignup{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", emailCheck='" + emailCheck + '\'' +
                '}';
    }
}
