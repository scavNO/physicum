package com.devbugger.physicum.domain.meal;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * These ingredients will always be saved with their raw integer
 * value for a gram for each hundred gram.
 *
 * There is no point to save data based on servings, as this is
 * not pre made food we are handling here. However, should the need
 * arise we can add a composite key between both "version" of the
 * ingredients and use it to represent both alternatives.
 */
@Entity
@Table(name = "ingredient")
public class Ingredient {

    @Id
    @Column(name = "ingredient_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "energy")
    private int energy;

    @Column(name = "protein")
    private int protein;

    @Column(name = "fat_total")
    private int fatTotal;

    @Column(name = "fat_saturated")
    private int fatSaturated;

    @Column(name = "carbohydrate")
    private int carbohydrate;

    @Column(name = "carbohydrate_sugar")
    private int carbohydrateSugar;

    @Column(name = "sodium")
    private int sodium;

    @JsonIgnore
    @ManyToMany(mappedBy = "ingredients", fetch = FetchType.LAZY)
    private Set<Meal> meals = new HashSet<>();

    public Ingredient() {

    }

    public Ingredient(String name, int energy, int protein, int fatTotal, int fatSaturated,
                      int carbohydrate, int carbohydrateSugar, int sodium) {
        this.name = name;
        this.energy = energy;
        this.protein = protein;
        this.fatTotal = fatTotal;
        this.fatSaturated = fatSaturated;
        this.carbohydrate = carbohydrate;
        this.carbohydrateSugar = carbohydrateSugar;
        this.sodium = sodium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFatTotal() {
        return fatTotal;
    }

    public void setFatTotal(int fatTotal) {
        this.fatTotal = fatTotal;
    }

    public int getFatSaturated() {
        return fatSaturated;
    }

    public void setFatSaturated(int fatSaturated) {
        this.fatSaturated = fatSaturated;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public int getCarbohydrateSugar() {
        return carbohydrateSugar;
    }

    public void setCarbohydrateSugar(int carbohydrateSugar) {
        this.carbohydrateSugar = carbohydrateSugar;
    }

    public int getSodium() {
        return sodium;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", protein=" + protein +
                ", fatTotal=" + fatTotal +
                ", fatSaturated=" + fatSaturated +
                ", carbohydrate=" + carbohydrate +
                ", carbohydrateSugar=" + carbohydrateSugar +
                ", sodium=" + sodium +
                '}';
    }
}
