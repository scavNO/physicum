package com.devbugger.physicum.domain.workout;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class WorkoutExerciseId implements Serializable {

    @ManyToOne
    public Workout workout;

    @ManyToOne
    public Exercise exercise;

    public WorkoutExerciseId() {

    }

    /**
     * Constructor used to create the composite key used for making
     * queries for the composite table. This key can be used to get
     * any matching entities from the composite table.
     * @param workout given for the query.
     * @param exercise given for the query.
     */
    public WorkoutExerciseId(Workout workout, Exercise exercise ) {
        this.workout = workout;
        this.exercise = exercise;
    }

//    @JsonBackReference(value = "workout")
    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

//    @JsonBackReference(value = "exercise")
    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkoutExerciseId that = (WorkoutExerciseId) o;

        if (workout != null ? !workout.equals(that.workout) : that.workout != null) return false;
        if (exercise != null ? !exercise.equals(that.exercise) : that.exercise != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = workout != null ? workout.hashCode() : 0;
        result = 31 * result + (exercise != null ? exercise.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WorkoutExerciseId{" +
                "workout=" + workout +
                ", exercise=" + exercise +
                '}';
    }
}