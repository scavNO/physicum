package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.workout.WorkoutCategory;
import com.devbugger.physicum.service.WorkoutService;

import java.beans.PropertyEditorSupport;

public class WorkoutCategoryEditior extends PropertyEditorSupport {

    private final WorkoutService workoutService;

    public WorkoutCategoryEditior(WorkoutService workoutService) {
        this.workoutService = workoutService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        WorkoutCategory workoutCategory =  workoutService.findCategoryById(Long.parseLong(text));
        setValue(workoutCategory);
    }
}
