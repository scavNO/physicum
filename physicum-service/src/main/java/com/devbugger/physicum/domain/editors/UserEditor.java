package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.service.UserService;

import java.beans.PropertyEditorSupport;

public class UserEditor extends PropertyEditorSupport {

    private final UserService userService;

    public UserEditor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        User user =  userService.findById(Long.parseLong(text));
        setValue(user);
    }
}
