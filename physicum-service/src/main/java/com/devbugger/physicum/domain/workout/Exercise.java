package com.devbugger.physicum.domain.workout;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "exercise")
public class Exercise {

    @Id
    @Column(name = "exercise_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

//    @JsonManagedReference(value = "muscle")
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="exercise_muscle",
            joinColumns=@JoinColumn(name="exercise_id"),
            inverseJoinColumns=@JoinColumn(name="muscle_id"))
    private Set<Muscle> muscles = new HashSet<>();

//    @JsonIgnore
//    @JsonManagedReference(value = "exercise")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.exercise",
            cascade = CascadeType.ALL)
    private Set<WorkoutExercise> workoutExercises = new HashSet<>();

    public Exercise() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Muscle> getMuscles() {
        return muscles;
    }

    public void setMuscles(Set<Muscle> muscles) {
        this.muscles = muscles;
    }

    public Set<WorkoutExercise> getWorkoutExercises() {
        return workoutExercises;
    }

    public void setWorkoutExercises(Set<WorkoutExercise> workoutExercises) {
        this.workoutExercises = workoutExercises;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exercise exercise = (Exercise) o;

        if (!id.equals(exercise.id)) return false;
        if (!name.equals(exercise.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", name='" + name + '\'' +
//                ", muscles=" + muscles +
//                ", workoutExercises=" + workoutExercises +
                '}';
    }

    //    @Override
//    public String toString() {
//        return "Exercise{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                '}';
//    }
}