package com.devbugger.physicum.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.metamodel.relational.Identifier;
import org.springframework.core.style.ToStringCreator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * ValidAuthority table
 *
 * Sets the ValidAuthority for Spring Security for controlling user access. *
 * These access flags can be set on both controllers and in spring-security.xml.
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 1/11/13
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "authority_id", scope = Identifier.class)
@Entity
@Table(name = "authority")
public class Authority {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "authority_id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "authority")
	private String authority;

	@ManyToMany(mappedBy = "authorities")
	private Set<User> users = new HashSet<>();

	public Authority() {

	}

	public Authority(String authority) {
		this.authority = authority;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", id)
                .append("name", name)
                .append("authority", authority)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Authority authority = (Authority) o;

        if (!id.equals(authority.id)) return false;
        if (name != null ? !name.equals(authority.name) : authority.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}