package com.devbugger.physicum.domain.workout;

import com.devbugger.physicum.domain.User;
import com.fasterxml.jackson.annotation.*;
import org.hibernate.metamodel.relational.Identifier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "workout")
public class Workout {

    @Id
    @Column(name = "workout_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "date")
    private Date date;

    @Column(name = "note_before")
    private String noteBefore;

    @Column(name = "note_after")
    private String noteAfter;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private WorkoutCategory workoutCategory;

//    @JsonManagedReference(value = "workout")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.workout",
            cascade = CascadeType.ALL)
    private Set<WorkoutExercise> workoutExercises = new HashSet<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Workout() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNoteBefore() {
        return noteBefore;
    }

    public void setNoteBefore(String noteBefore) {
        this.noteBefore = noteBefore;
    }

    public String getNoteAfter() {
        return noteAfter;
    }

    public void setNoteAfter(String noteAfter) {
        this.noteAfter = noteAfter;
    }

    public WorkoutCategory getWorkoutCategory() {
        return workoutCategory;
    }

    public void setWorkoutCategory(WorkoutCategory workoutCategory) {
        this.workoutCategory = workoutCategory;
    }

    public Set<WorkoutExercise> getWorkoutExercises() {
        return workoutExercises;
    }

    public void setWorkoutExercises(Set<WorkoutExercise> workoutExercises) {
        this.workoutExercises = workoutExercises;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workout workout = (Workout) o;

        if (!date.equals(workout.date)) return false;
        if (!id.equals(workout.id)) return false;
        if (!name.equals(workout.name)) return false;
        if (noteAfter != null ? !noteAfter.equals(workout.noteAfter) : workout.noteAfter != null) return false;
        if (noteBefore != null ? !noteBefore.equals(workout.noteBefore) : workout.noteBefore != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", noteBefore='" + noteBefore + '\'' +
                ", noteAfter='" + noteAfter + '\'' +
//                ", workoutCategory=" + workoutCategory +
//                ", workoutExercises=" + workoutExercises +
                ", user=" + user +
                '}';
    }

    //    @Override
//    public String toString() {
//        return "Workout{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", date=" + date +
//                ", noteBefore='" + noteBefore + '\'' +
//                ", noteAfter='" + noteAfter + '\'' +
//                ", workoutCategory=" + workoutCategory +
//                '}';
//    }
}
