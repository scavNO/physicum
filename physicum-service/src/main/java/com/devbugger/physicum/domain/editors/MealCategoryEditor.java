package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.meal.MealCategory;
import com.devbugger.physicum.service.MealCategoryService;

import java.beans.PropertyEditorSupport;

public class MealCategoryEditor  extends PropertyEditorSupport {

    private final MealCategoryService mealCategoryService;

    public MealCategoryEditor(MealCategoryService mealCategoryService) {
        this.mealCategoryService = mealCategoryService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        MealCategory mealCategory =  mealCategoryService.findById(Long.parseLong(text));
        setValue(mealCategory);
    }
}