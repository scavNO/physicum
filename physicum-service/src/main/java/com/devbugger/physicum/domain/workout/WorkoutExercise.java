package com.devbugger.physicum.domain.workout;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

//@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "workout_exercise")
@AssociationOverrides(value = {
        @AssociationOverride(name = "pk.workout",
                joinColumns = @JoinColumn(name = "workout_id", updatable = true, insertable = true)),
        @AssociationOverride(name = "pk.exercise",
                joinColumns = @JoinColumn(name = "exercise_id", updatable = true, insertable = true))
})
public class WorkoutExercise {

    @EmbeddedId
    private WorkoutExerciseId pk = new WorkoutExerciseId();

    @NotNull
    @Column(name = "sets")
    private int sets;

    @NotNull
    @Column(name = "reps")
    private int reps;

    @NotNull
    @Column(name = "weight")
    private double weight;

    public WorkoutExerciseId getPk() {
        return pk;
    }

    public void setPk(WorkoutExerciseId pk) {
        this.pk = pk;
    }

    @Transient
//    @JsonManagedReference("workout")
    public Workout getWorkout() {
        return getPk().getWorkout();
    }

    public void setWorkout(Workout workout) {
        getPk().setWorkout(workout);
    }

    @Transient
//    @JsonManagedReference("exercise")
    public Exercise getExercise() {
        return getPk().getExercise();
    }

    public void setExercise(Exercise exercise) {
        getPk().setExercise(exercise);
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkoutExercise that = (WorkoutExercise) o;

        if(getPk() != null ? !getPk().equals(that.getPk()): that.getPk() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }

    @Override
    public String toString() {
        return "WorkoutExercise{" +
                "pk=" + pk +
                ", sets=" + sets +
                ", reps=" + reps +
                ", weight=" + weight +
                '}';
    }
}
