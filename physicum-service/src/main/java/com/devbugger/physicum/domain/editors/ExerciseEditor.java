package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.service.ExerciseService;

import java.beans.PropertyEditorSupport;

public class ExerciseEditor  extends PropertyEditorSupport {

    private final ExerciseService exerciseService;

    public ExerciseEditor(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Exercise exercise =  exerciseService.findById(Long.parseLong(text));
        setValue(exercise);
    }
}