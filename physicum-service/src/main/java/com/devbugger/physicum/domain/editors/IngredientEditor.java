package com.devbugger.physicum.domain.editors;

import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.service.MealService;

import java.beans.PropertyEditorSupport;

/**
 * Editor using {@link com.devbugger.physicum.service.MealService} to provide
 * editorial functionality.
 */
public class IngredientEditor extends PropertyEditorSupport {

    private final MealService mealService;

    public IngredientEditor(MealService mealService) {
        this.mealService = mealService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Ingredient ingredient =  mealService.ingredientFindById(Long.parseLong(text));
        setValue(ingredient);
    }
}
