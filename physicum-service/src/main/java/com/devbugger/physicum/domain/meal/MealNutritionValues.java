package com.devbugger.physicum.domain.meal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Set;

/**
 * Class intended to be transient and only retain values based
 * on in memory access by providing service layer data transformation.
 */
public class MealNutritionValues {

    private int energy;
    private int protein;
    private int fatTotal;
    private int fatSaturated;
    private int carbohydrate;
    private int carbohydrateSugar;
    private int sodium;

    @JsonIgnore
    private Set<Ingredient> ingredientList;

    public MealNutritionValues(Set<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
        sum();
    }

    private void sum() {
        ingredientList.forEach((ingredient) -> {
            addProtein(ingredient.getProtein());
            addCarbohydrate(ingredient.getCarbohydrate());
            addCarbohydrateSugar(ingredient.getCarbohydrateSugar());
            addEnergy(ingredient.getEnergy());
            addFatSaturated(ingredient.getFatSaturated());
            addFatTotal(ingredient.getFatTotal());
            addSodium(ingredient.getSodium());
        });
    }

    public int getEnergy() {
        return energy;
    }

    public void addEnergy(int energy) {
        this.energy += energy;
    }

    public int getProtein() {
        return protein;
    }

    public void addProtein(int protein) {
        this.protein =+ protein;
    }

    public int getFatTotal() {
        return fatTotal;
    }

    public void addFatTotal(int fatTotal) {
        this.fatTotal =+ fatTotal;
    }

    public int getFatSaturated() {
        return fatSaturated;
    }

    public void addFatSaturated(int fatSaturated) {
        this.fatSaturated =+ fatSaturated;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void addCarbohydrate(int carbohydrate) {
        this.carbohydrate =+ carbohydrate;
    }

    public int getCarbohydrateSugar() {
        return carbohydrateSugar;
    }

    public void addCarbohydrateSugar(int carbohydrateSugar) {
        this.carbohydrateSugar =+ carbohydrateSugar;
    }

    public int getSodium() {
        return sodium;
    }

    public void addSodium(int sodium) {
        this.sodium =+ sodium;
    }

    @Override
    public String toString() {
        return "MealNutritionValues{" +
                "energy=" + energy +
                ", protein=" + protein +
                ", fatTotal=" + fatTotal +
                ", fatSaturated=" + fatSaturated +
                ", carbohydrate=" + carbohydrate +
                ", carbohydrateSugar=" + carbohydrateSugar +
                ", sodium=" + sodium +
                ", ingredientList=" + ingredientList +
                '}';
    }
}