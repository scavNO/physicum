package com.devbugger.physicum.security;

import com.devbugger.physicum.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of Spring Security UserDetails class.
 * It basically just holds all the information Spring Security
 * needs to provide application level access.
 * @author dag
 *
 */
public class PhysicumUserDetails implements UserDetails {
	private static final long serialVersionUID = -2985656388091941799L;
	
	private User user;
	private List<? extends GrantedAuthority> authorities = new ArrayList<>();
	
	public PhysicumUserDetails(User user, List<? extends GrantedAuthority> authorities) {
		this.user = user;
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	public User getUser() {
		return user;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
