package com.devbugger.physicum.security;

/**
 * Defines the ValidAuthority flags in the application to avoid
 * spelling them wrong.
 *
 * If changes are made to this file or its filename, spring-security.xml
 * needs to be updated to reflect these changes.
 * @author dag
 *
 */
public enum ValidAuthority {
    ROLE_NEW,
	ROLE_USER,
	ROLE_ADMIN,
}
