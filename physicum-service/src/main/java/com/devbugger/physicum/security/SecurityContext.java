package com.devbugger.physicum.security;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Spring Security context. Provides access to the details
 * regarding the currently logged in user and can be used throughout
 * the application.
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 11/16/13
 */
public class SecurityContext {
	
	public static PhysicumUserDetails getUserDetails() {
		return (PhysicumUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}