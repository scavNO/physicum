package com.devbugger.physicum.security;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Set up the user with all authorities added to the user
 * as defined in the database.
 * @author dag
 *
 */
@Component
public class PhysicumUserDetailsService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(PhysicumUserDetailsService.class);
	
	@Autowired
	private UserService userService;

    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Trying to log in with user [" + username + "]");
		if(StringUtils.isBlank(username)) {
            logger.info("Username is empty!");
			throw new UsernameNotFoundException("Username empty!");
		}
		
		User user = userService.findByUserName(username);
		
		if(user == null) {
            logger.info("Username (" + username + ")not found!");
			throw new UsernameNotFoundException("Username not found!");
		}
		
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

		logger.info("Fetching and assigning authorities to user");
		for(Authority authority : user.getAuthorities()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(authority.getAuthority()));
			logger.info(authority.getAuthority());
		}
		
		return new PhysicumUserDetails(userService.findByUserName(username), grantedAuthorities);
	}
}