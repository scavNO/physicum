package com.devbugger.physicum.utils;

import com.devbugger.physicum.security.SecurityContext;

/**
 * Generates tokens and such for the application
 * Author: Dag Østgulen Heradstveit
 * Date: 2/19/14
 * Time: 12:27 AM
 */
public class RandomUtils {

    /**
     * Generates a medium length random string. Not very safe.
     * @return
     */
    public static String generateUnsafe() {
        return Long.toHexString(Double.doubleToLongBits(Math.random()));
    }

    /**
     * Generate a medium length safe random String.
     * @return
     */
    public static String generateSafe() {
        String userToken = String.valueOf(SecurityContext.getUserDetails().getUser().hashCode());
        return Long.toHexString(Double.doubleToLongBits(Math.random()))+userToken;
    }
}
