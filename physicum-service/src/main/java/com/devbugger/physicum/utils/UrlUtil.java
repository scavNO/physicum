package com.devbugger.physicum.utils;

import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

/**
 * Encode urls for redirecting to the preferred path.
 * @author dag
 */

public class UrlUtil {

	public static String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
		String encoding = httpServletRequest.getCharacterEncoding();
		
		if(encoding == null) {
			encoding = WebUtils.DEFAULT_CHARACTER_ENCODING;
		}
		
		try {
			pathSegment = UriUtils.encodePathSegment(pathSegment, encoding);
		}
		
		catch (UnsupportedEncodingException e) {
			//Do nothing.
		}
		
		return pathSegment;
	}

}
