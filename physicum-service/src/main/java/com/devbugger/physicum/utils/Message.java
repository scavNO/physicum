package com.devbugger.physicum.utils;


/**
 * Sends messages across the system for the different views.
 * Message is specified in the controller and showed in the
 * form where it applies when triggered.
 * 
 * It support different types of messages to make it easier
 * to separate warnings from information and so on.
 * 
 * Includes a JQuery snippet to remove the message after a given time.
 * 
 * @author dag
 *
 */
public class Message {
	
	private String messageType;
	private String messageText;
	
	//Set the time in milliseconds for the message to disappear
	private int delayTimer = 1500;
	private String JQueryMessage = 	"<script>\n"+
										"$(document).ready( function() {\n"+
											"+$('#message').delay("+ delayTimer +").fadeOut();\n"+
										"});\n"+
									"</script>\n";
	
	/**
	 * Empty default constructor. Should not be called.
	 */
	public Message() {
		
	}
	
	/**
	 * Constructor for actual messages. Should be used when ever
	 * a proper message will be sent accross the system.
	 * @param messageType
	 * @param messageText
	 */
	public Message(String messageType, String messageText) {
		this.messageType = messageType;
		this.messageText = messageText + JQueryMessage;
	}
	
	/**
	 * @return messageType
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * 
	 * @return messageText
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * Set the text message, including a JQuery snippet to remove
	 * it after a certain time.
	 * 
	 * @param messageText
	 */
	public void setMessageText(String messageText) {
		this.messageText += messageText;
	}

}
