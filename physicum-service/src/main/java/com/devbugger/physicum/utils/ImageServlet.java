package com.devbugger.physicum.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;

/** *
 * Simple servlet for serving images from the OS' filesystem.
 *
 * Based on @link http://balusc.blogspot.com/2007/04/imageservlet.html
 * @author BalusC
 * @author Dag Østgulen Heradstveit
 */
public class ImageServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ImageServlet.class);

    private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.
    public static final String IMAGE_SERVLET_ROOT_DIR = "/images/";
    public static final String SYSTEM_ROOT_DIR = "/home/dag/Apps/familyboard/img/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        logger.info("Getting image and doing data validation");
        String requestedImage = request.getPathInfo();

        if (requestedImage == null) {
            logger.info("Servlet does not accept null requests.");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        File image = new File(SYSTEM_ROOT_DIR, URLDecoder.decode(requestedImage, "UTF-8"));

        if (!image.exists()) {
            logger.info("This image does not exist.");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        String contentType = getServletContext().getMimeType(image.getName());
        if (contentType == null || !contentType.startsWith("image")) {
            logger.info("Resource has no type or is not an image.");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        createResponse(response, contentType, image);
    }

    private void createResponse(HttpServletResponse response, String contentType, File image) throws IOException {
        logger.info("Creating response");
        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(image.length()));
        response.setHeader("Content-Disposition", "inline; filename=\"" + image.getName() + "\"");

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(image), DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            // Write file contents to response.
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        }

        finally {
            close(output);
            close(input);
        }
    }


    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
                logger.info("Closing request resource");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
