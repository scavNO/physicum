package com.devbugger.physicum.json;

import com.devbugger.physicum.domain.workout.Muscle;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Represents a {@link com.devbugger.physicum.domain.workout.Workout} with its
 * {@link com.devbugger.physicum.domain.workout.WorkoutExercise} and all of the
 * {@link com.devbugger.physicum.domain.workout.Exercise} following it.
 */
public class WorkoutSerializer extends JsonSerializer<Workout> {

    private static final Logger logger = LoggerFactory.getLogger(WorkoutSerializer.class);

    @Override
    public void serialize(Workout workout, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        if(workout == null) logger.error("Error parsing json!");

        if(workout != null) {

            //Workout
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("id", workout.getId().toString());
            jsonGenerator.writeStringField("name", workout.getName());
            jsonGenerator.writeStringField("noteBefore", workout.getNoteBefore());
            jsonGenerator.writeStringField("noteAfter", workout.getNoteAfter());

            //Exercises
            jsonGenerator.writeArrayFieldStart("exercises");
            for(WorkoutExercise workoutExrecise : workout.getWorkoutExercises()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("id", workoutExrecise.getExercise().getId().toString());
                jsonGenerator.writeStringField("name", workoutExrecise.getExercise().getName());
                jsonGenerator.writeStringField("reps", String.valueOf(workoutExrecise.getReps()));
                jsonGenerator.writeStringField("sets", String.valueOf(workoutExrecise.getSets()));
                jsonGenerator.writeStringField("weight", String.valueOf(workoutExrecise.getWeight()));
                jsonGenerator.writeEndObject();

                //Muscles
                jsonGenerator.writeArrayFieldStart("muscles");
                for(Muscle muscle : workoutExrecise.getExercise().getMuscles()) {
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeStringField("id", muscle.getId().toString());
                    jsonGenerator.writeStringField("name", muscle.getName());
                    jsonGenerator.writeEndObject();
                }

                //End Muscles
                jsonGenerator.writeEndArray();

            }

            //End Exercises
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
    }
}
