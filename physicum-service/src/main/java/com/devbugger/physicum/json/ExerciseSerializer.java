package com.devbugger.physicum.json;

import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.domain.workout.Muscle;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ExerciseSerializer extends JsonSerializer<Exercise> {

    private static final Logger logger = LoggerFactory.getLogger(ExerciseSerializer.class);

    @Override
    public void serialize(Exercise exercise, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        if (exercise == null) logger.error("Error parsing json!");

        if (exercise != null) {

            //Workout
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("id", exercise.getId().toString());
            jsonGenerator.writeStringField("name", exercise.getName());

            //muscles
            jsonGenerator.writeArrayFieldStart("muscles");

            for (Muscle muscle : exercise.getMuscles()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("id", muscle.getId().toString());
                jsonGenerator.writeStringField("name", muscle.getName());
                jsonGenerator.writeEndObject();
            }

            //End muscles
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
    }
}