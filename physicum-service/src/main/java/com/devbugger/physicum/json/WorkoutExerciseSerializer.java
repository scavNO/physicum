package com.devbugger.physicum.json;

import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class WorkoutExerciseSerializer extends JsonSerializer<WorkoutExercise> {

    private static final Logger logger = LoggerFactory.getLogger(WorkoutExerciseSerializer.class);

    @Override
    public void serialize(WorkoutExercise workoutExercise, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        if(workoutExercise == null) logger.error("Error parsing json!");

        if(workoutExercise != null) {

            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("id", workoutExercise.getWorkout().getId().toString());
            jsonGenerator.writeStringField("name", workoutExercise.getWorkout().getName());
            jsonGenerator.writeStringField("description", workoutExercise.getWorkout().getNoteBefore());
            jsonGenerator.writeStringField("startDate", workoutExercise.getWorkout().getNoteAfter());

            jsonGenerator.writeArrayFieldStart("workoutExercises");
            //Waiting for Java 9 before using Lambdas here.
            for(WorkoutExercise aWorkoutExrecise : workoutExercise.getExercise().getWorkoutExercises()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("id", aWorkoutExrecise.getExercise().getId().toString());
                jsonGenerator.writeStringField("name", aWorkoutExrecise.getExercise().getName());
                jsonGenerator.writeStringField("reps", String.valueOf(aWorkoutExrecise.getReps()));
                jsonGenerator.writeStringField("sets", String.valueOf(aWorkoutExrecise.getSets()));
                jsonGenerator.writeStringField("weight", String.valueOf(aWorkoutExrecise.getWeight()));
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();

            jsonGenerator.writeEndObject();
        }
    }
}