package com.devbugger.physicum.configuration;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * This class just hold packages that should be scanned
 * for Spring annotated classes.
 */
@Configuration
@ComponentScan(basePackages = {
        "com.devbugger.physicum.controller",
        "com.devbugger.physicum.service",
        "com.devbugger.physicum.security",
        "com.devbugger.physicum.domain"})
public class ComponentConfiguration {
}
