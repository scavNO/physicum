package com.devbugger.physicum.configuration;

import com.devbugger.physicum.configuration.support.PhysicumProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;

/**
 * Configure views based on the given {@link com.devbugger.physicum.configuration.support.PhysicumProfile}
 * being selected by supplying -Dspring.profiles.active="$PROFILE" on start.
 *
 * This view class does very little, besides interacting with the root index.html file.
 */
@Configuration
public class ViewConfiguration {

    @Autowired
    private MessageSource messageSource;

    @Profile(PhysicumProfile.PRODUCTION)
    @Bean(name = "resourceResolver")
    public SpringResourceTemplateResolver productionResourceResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setPrefix("classpath:/META-INF/views/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setCacheable(false);

        return resolver;
    }

    @Value("${application.dir}")
    private String applicationDir;

    @Profile(PhysicumProfile.DEVELOPMENT)
    @Bean(name = "templateResolver")
    public FileTemplateResolver developmentResourceResolver() {
        FileTemplateResolver resolver = new FileTemplateResolver();
        resolver.setPrefix(applicationDir+"/views/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setCacheable(false);

        return resolver;
    }

    @Profile(PhysicumProfile.PRODUCTION)
    @Bean(name = "viewResolver")
    public ViewResolver viewResolverProduction() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngineProduction());
        viewResolver.setOrder(1);
        viewResolver.setViewNames(new String[]{"*"});
        viewResolver.setCache(false);
        return viewResolver;
    }

    @Profile(PhysicumProfile.DEVELOPMENT)
    @Bean(name = "viewResolver")
    public ViewResolver viewResolverDevelopment() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngineDevelopment());
        viewResolver.setOrder(1);
        viewResolver.setViewNames(new String[]{"*"});
        viewResolver.setCache(false);
        return viewResolver;
    }

    public SpringTemplateEngine templateEngineProduction() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(productionResourceResolver());
        engine.setMessageSource(messageSource);
        return engine;
    }

    public SpringTemplateEngine templateEngineDevelopment() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(developmentResourceResolver());
        engine.setMessageSource(messageSource);
        return engine;
    }


}