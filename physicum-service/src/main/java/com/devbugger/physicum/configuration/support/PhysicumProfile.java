package com.devbugger.physicum.configuration.support;

public class PhysicumProfile {
    public final static String DEVELOPMENT = "development";
    public final static String PRODUCTION = "production";
}
