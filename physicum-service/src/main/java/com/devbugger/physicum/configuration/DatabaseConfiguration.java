package com.devbugger.physicum.configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.support.OpenSessionInViewFilter;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Contains all the Beans and configuration needed to
 * provide database access to the application as well as
 * defining the pooling and connection properties.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.devbugger.physicum.repository")
public class DatabaseConfiguration {

    @Value("${database.url}")
    private String dbUrl;

    @Value("${database.user}")
    private String dbUser;

    @Value("${database.pass}")
    private String dbPass;

    @Value("${database.driver}")
    private String dbDriver;

    private final String packagesToScan = "com.devbugger.physicum.domain";
    private final String hibernateProperties = "hibernate.properties";

    /**
     * The entity manager, manages all the (database) entities in the system.
     * @return LocalContainerEntityManagerFactoryBean
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        LocalContainerEntityManagerFactoryBean emf
                = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan(packagesToScan);

        emf.setPersistenceProvider(hibernateJpaVendorAdapter());
        emf.setJpaProperties(getHibernateProperties());

        return emf;
    }

    private HibernatePersistenceProvider hibernateJpaVendorAdapter() {
        HibernatePersistenceProvider vendorAdapter = new HibernatePersistenceProvider();

        return vendorAdapter;
    }

    /**
     * Sets the datasource to a c3p0 pooled one.
     * @return DataSource
     */
    @Bean
    public DataSource dataSource(){
        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        try {
            dataSource.setDriverClass(dbDriver);
            dataSource.setJdbcUrl(dbUrl);
            dataSource.setUser(dbUser);
            dataSource.setPassword(dbPass);
            dataSource.setMaxPoolSize(10);
            dataSource.setMinPoolSize(2);
            dataSource.setInitialPoolSize(2);
            dataSource.setAutoCommitOnClose(true);

        }

        catch (PropertyVetoException e) {
            e.printStackTrace();
        }

        return dataSource;
    }

    /**
     * Transaction manager to provide transaction support.
     * @return JpaTransactionManager
     */
    @Bean
    public JpaTransactionManager transactionManager(){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(this.entityManagerFactory().getObject());

        //Change:
        transactionManager.setDataSource(this.dataSource());

        return transactionManager;
    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Fetches the hibernate properties from a standard
     * Java properties file.
     * @return HibernateProperties
     */
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream(hibernateProperties);
        try {
            props.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return props;
    }

    /**
     * Ensures that the session(connection) to the database is active
     * while rendering view, if not, we will get a NPE on the data.
     * @return OpenSessionInViewFilter
     */
    @Bean
    public OpenSessionInViewFilter openSessionInViewFilter() {
        OpenSessionInViewFilter openSessionInViewFilter = new OpenSessionInViewFilter();
        openSessionInViewFilter.setSessionFactoryBeanName("sessionFactory");

        return openSessionInViewFilter;
    }

}
