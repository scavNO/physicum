package com.devbugger.physicum.configuration;

import com.devbugger.physicum.configuration.support.PhysicumProfile;
import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.json.DateSerializer;
import com.devbugger.physicum.json.ExerciseSerializer;
import com.devbugger.physicum.json.WorkoutSerializer;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.ui.context.ThemeSource;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ThemeResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Configuration
@EnableWebMvc
public class WebMvcContextConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment environment;
	
    @Autowired
    private DatabaseConfiguration database;

    /**
     * Overides the addInterceptor method to provide a registry of the different
     * implementation of interceptors within the application.
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(localeChangeInterceptor());

        /**
         * Keeps the Hibernate session open while the view is being rendered.
         */
        OpenEntityManagerInViewInterceptor openEntityManagerInViewInterceptor =
                new OpenEntityManagerInViewInterceptor();
        openEntityManagerInViewInterceptor.setEntityManagerFactory(
                database.entityManagerFactory().getObject());
        registry.addWebRequestInterceptor(openEntityManagerInViewInterceptor);

//        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
//        characterEncodingFilter.setEncoding("UTF-8");
//        characterEncodingFilter.setForceEncoding(true);r);
    }


    /**
     * Creates the resources of Javascript and CSS that will be
     * available to all views within the application.
     *
     * These are defined by either of the profiles found in
     * {@link com.devbugger.physicum.configuration.support.PhysicumProfile} and
     * activated by -Dspring.profiles.active="$PROFILE" on start.
     * @param registry the registry of resources
     */
    @Value("${application.dir}")
    private String applicationDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String resourcePath = null;
        for(String s : environment.getActiveProfiles()) {
            if(s.equals(PhysicumProfile.DEVELOPMENT)) {
                resourcePath = "file:"+this.applicationDir;
                registry.addResourceHandler("/bower_components/**")
                        .addResourceLocations(resourcePath+"/bower_components/")
                        .setCachePeriod(0);
            }
            else if(s.equals(PhysicumProfile.PRODUCTION)) {
                resourcePath = "classpath:/META-INF";
            }
        }

        registry.addResourceHandler("/views/**")
                .addResourceLocations(resourcePath+"/views/")
                .setCachePeriod(0);
        registry.addResourceHandler("/styles/**")
                .addResourceLocations(resourcePath+"/styles/")
                .setCachePeriod(0);
        registry.addResourceHandler("/fonts/**")
                .addResourceLocations(resourcePath+"/fonts/")
                .setCachePeriod(0);
        registry.addResourceHandler("/scripts/**")
                .addResourceLocations(resourcePath+"/scripts/")
                .setCachePeriod(0);
    }

    /**
     * Add MessageConverters to the application.
     * @param converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jacksonMessageConverter());
    }

    /**
     * HttpMessageConverter for MappingJackson2HttpMessageConverter
     * @return
     */
    public HttpMessageConverter jacksonMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setPrefixJson(false);
        converter.setPrettyPrint(true);
        converter.setObjectMapper(objectMapper());
        return converter;
    }

    /**
     * Register all the object mappers used within the application
     * to make them available for the HttpMessageConverter.
     * @return ObjectMapper
     */
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        SimpleModule module = new SimpleModule("serialzer", Version.unknownVersion());
        module.addSerializer(Date.class, new DateSerializer());
        module.addSerializer(Workout.class, new WorkoutSerializer());
        module.addSerializer(Exercise.class, new ExerciseSerializer());
        objectMapper.registerModule(module);

        return objectMapper;
    }

    /**
     * Intercept changes in the locale settings.
     * It works by appending ?lang={LOCALE_FLAG} (en_GB) to the url and
     * this is intercepted by the servlet.
     * @return localeChangeInterceptor
     */
    @Bean
    public HandlerInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor localeChangeInterceptor =
                new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");

        return localeChangeInterceptor;
    }

    /**
     * Resolve the locale of the users system, and puts it in a cookie,
     * providing default locale for the user based on system settings.
     * @return localeResolver
     */
    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setCookieName("locale");
        localeResolver.setDefaultLocale(new Locale("en_GB"));

        return localeResolver;
    }

    /**
     * Bean to provide access to the different message bundles
     * within the system. Provides both messages and text labels
     * for the application.
     * @return messageSource
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource =
                new ReloadableResourceBundleMessageSource();

        messageSource.setBasenames("WEB-INF/i18n/messages", "WEB-INF/i18n/application");
//        messageSource.setFallbackToSystemLocale(false);

        return messageSource;
    }



    /**
     * Provides a resolver for the Theme found in /WEB-INF/classes.
     * @return themeResolver
     */
    @Bean
    public ThemeResolver themeResolver() {
        CookieThemeResolver themeResolver = new CookieThemeResolver();
        themeResolver.setDefaultThemeName("standard");
        themeResolver.setCookieName("defaultTheme");

        return themeResolver;
    }

    /**
     * The theme resource.
     * @return new ResourceBundleThemeSource
     */
    @Bean
    public ThemeSource themeSource() {
        return new ResourceBundleThemeSource();
    }

    /**
     * Validate stuff and uses the messageSource to send the validation
     * messages.
     *
     * @return validator
     */
    @Bean
    public Validator validator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource());

        return validator;
    }

    /**
     * Add support for file upload.
     * @return
     */
    @Bean
    public MultipartResolver multipartResolver() {
        int MAX_FILE_SIZE = 1024 * 1024 * 10;
        int WRITE_TO_DISK_THRESHOLD = 1024 * 1024;
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxInMemorySize(WRITE_TO_DISK_THRESHOLD);
        multipartResolver.setMaxUploadSize(MAX_FILE_SIZE);

        return multipartResolver;
    }

    /**
     * Currently not in use. Can be used to provide Content Negotiation
     * but this is not needed at the moment.
     */
//    @Override
//    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//        configurer.favorPathExtension(false).
//                favorParameter(false).
//                parameterName("mediaType").
//                ignoreAcceptHeader(true).
//                useJaf(false).
//                defaultContentType(MediaType.TEXT_HTML).
//                mediaType("xml", MediaType.APPLICATION_XML).
//                mediaType("json", MediaType.APPLICATION_JSON);
//    }
}
