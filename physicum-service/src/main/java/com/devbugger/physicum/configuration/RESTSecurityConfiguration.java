package com.devbugger.physicum.configuration;

import com.devbugger.physicum.security.PhysicumUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Contains the security settings for restful and stateless based authentication
 * to provide access to protected RESTful API's.
 */
@Order(1)
@Configuration
@EnableWebSecurity
public class RESTSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final static String REALM_NAME = "Physicum RESTful API";
    private final static String API_URL = "/api/**";

    @Autowired
    PhysicumUserDetailsService physicumUserDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.physicumUserDetailsService).passwordEncoder(passwordEncoder);
    }

    /**
     * Set up basic http authentication which is useful for
     * non-web clients connecting to the service to fetch and
     * post data.
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .requestMatchers()
                .antMatchers(API_URL)
            .and()
                .authorizeRequests()
                .antMatchers(API_URL).hasRole("USER")
            .and()
                .httpBasic().realmName(REALM_NAME);
    }
}