package com.devbugger.physicum.configuration;

import com.devbugger.physicum.security.PhysicumUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Contains the security settings for form based authentication.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    PhysicumUserDetailsService physicumUserDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * Default {@link org.springframework.security.crypto.password.PasswordEncoder} using the newer and
     * generally better BCrypt password algorithm.
     * @return application wide password encoder.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.physicumUserDetailsService).passwordEncoder(passwordEncoder);
    }

    /**
     * Set up folders and paths Spring Security should ignore.
     * This is useful to avoid overhead and to make each call
     * involving static resources much quicker.
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
                .antMatchers("/bower_components/**")
                .antMatchers("/scripts/**")
                .antMatchers("/styles/**")
                .antMatchers("/fonts/**")
                .antMatchers("/images/**")
                .antMatchers("/resources/**")
                .antMatchers("/favicon.ico");
    }

    /**
     * Set up protected resources and form based authentication.
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .authorizeRequests()
                .antMatchers("/signup").anonymous()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/public/**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/api/**").hasRole("USER")
                .antMatchers("/api/**").hasRole("ADMIN")
                .antMatchers("/**").hasAnyRole("USER", "ADMIN")
            .and()
                .csrf().disable()
                .formLogin()
                .loginProcessingUrl("/j_spring_security_check")
                .loginPage("/auth").failureUrl("/auth")
                .usernameParameter("j_username").passwordParameter("j_password")
                .defaultSuccessUrl("/")
            .and()
                .logout()
                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl("/auth");
    }
}