package com.devbugger.physicum.configuration;

import org.springframework.context.annotation.Configuration;

/**
 * Contains all session beans in the application.
 *
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 11/1/13
 */
@Configuration
public class SessionBeans {
	
//    @Bean
//    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
//    public DataObject dataObject() {
//        return new DataObject();
//    }

}
