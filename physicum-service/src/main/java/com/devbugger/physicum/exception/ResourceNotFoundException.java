package com.devbugger.physicum.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom Exception for resources not found.
 * Based on the Spring implementation.
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 11/1/13
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -2229457122999294232L;
	
	private Long resourceId;
	
	public ResourceNotFoundException(Long resourceId) {
		this.resourceId = resourceId;
	}
	
	public Long getId() {
		return resourceId;
	}	
}
