package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.workout.Workout;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkoutRepository extends JpaRepository<Workout, Long> {

    public List<Workout> findAllByUser(User user);
}
