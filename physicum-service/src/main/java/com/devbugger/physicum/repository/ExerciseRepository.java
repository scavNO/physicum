package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.workout.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {

    public List<Exercise> findByNameStartingWith(String name);
}
