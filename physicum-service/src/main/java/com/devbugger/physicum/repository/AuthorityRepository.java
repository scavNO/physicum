package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.Authority;
import org.springframework.data.repository.CrudRepository;

public interface  AuthorityRepository extends CrudRepository<Authority, Long> {

    public Authority findByAuthority(String validAuthority);
}
