package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.domain.meal.MealCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MealRepository extends JpaRepository<Meal, Long> {

    /**
     * Get the latest meals by the user. The amount should be set
     * to 5, as this is a nice and round number.
     * @param user the user we want the meals from
     * @param pageable pages and limit
     * @return a list of the users meals, in descending order.
     */
    List<Meal> findMealByUserOrderByIdDesc(User user, Pageable pageable);

    List<Meal> findMealByMealCategory(MealCategory mealCategory);
}
