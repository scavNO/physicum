package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.meal.MealCategory;
import org.springframework.data.repository.CrudRepository;

public interface MealCategoryRepository extends CrudRepository<MealCategory, Long> {
}
