package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.workout.WorkoutCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkoutCategoryRepository extends JpaRepository<WorkoutCategory, Long> {
}
