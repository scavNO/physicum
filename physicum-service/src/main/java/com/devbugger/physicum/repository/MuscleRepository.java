package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.workout.Muscle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MuscleRepository extends JpaRepository<Muscle, Long> {
}
