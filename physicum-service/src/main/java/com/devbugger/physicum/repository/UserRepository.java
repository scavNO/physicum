package com.devbugger.physicum.repository;


import com.devbugger.physicum.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
	
	/**
	 * Finds a by its username.
	 * @param username
	 * @return
	 */
	public User findUserByUsername(String username);

}
