package com.devbugger.physicum.repository;

import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.devbugger.physicum.domain.workout.WorkoutExerciseId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkoutExerciseRepository extends JpaRepository<WorkoutExercise, WorkoutExerciseId> {

    List<WorkoutExercise> findByPkWorkout(Workout workout);

    List<WorkoutExercise> findByPkExercise(Exercise exercise);

}
