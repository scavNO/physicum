package com.devbugger.physicum.repository;


import com.devbugger.physicum.domain.meal.Ingredient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

    public List<Ingredient> findByNameStartingWith(String name);

}
