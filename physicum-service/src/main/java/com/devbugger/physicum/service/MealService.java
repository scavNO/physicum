package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.domain.meal.MealCategory;

import java.util.List;

public interface MealService {

    /**
     * Find one {@link com.devbugger.physicum.domain.meal.Meal} and calculate its
     * {@link com.devbugger.physicum.domain.meal.MealNutritionValues} before returning the object.
     * @param id of the meal.
     * @return the complete Meal object.
     */
    public Meal findById(Long id);

    /**
     * Return a page result list of {@link com.devbugger.physicum.domain.meal.Meal} objects and
     * calculate their {@link com.devbugger.physicum.domain.meal.MealNutritionValues}
     * @param user requesting the data
     * @param page the page requested
     * @param pageSize the size of the returned pages
     * @return the complete list of the selected page
     */
    public List<Meal> pageableMealList(User user, int page, int pageSize);

    public List<Meal> findByMealCategory(MealCategory mealCategory);

    public Meal save(Meal meal);

    public void delete(Meal meal);

    public List<Ingredient> ingredientFindAll();

    public Ingredient ingredientFindById(Long id);

    public List<Ingredient> ingredientsFindByName(String name);

    public Ingredient ingredientSave(Ingredient ingredient);

}
