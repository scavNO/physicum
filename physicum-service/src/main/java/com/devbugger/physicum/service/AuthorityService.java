package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.security.ValidAuthority;

import java.util.List;

/**
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 11/1/13
 */
public interface AuthorityService {

    /**
     * Find one Authority based on its id.
     * @param id
     * @return
     */
    public Authority findById(Long id);

    /**
     * Fetches all roles in the system.
     * @return authority
     */
    public List<Authority> findAll();

    /**
     * Find one authority by inputting a valid authority enum.
     * @param validAuthority
     * @return
     */
    public Authority findByAuthority(ValidAuthority validAuthority);

}
