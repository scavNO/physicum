package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.domain.meal.MealCategory;
import com.devbugger.physicum.domain.meal.MealNutritionValues;
import com.devbugger.physicum.repository.IngredientRepository;
import com.devbugger.physicum.repository.MealRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Service class containing all methods for Meals.
 * This include all related Entity classes for the domain.
 * {@link com.devbugger.physicum.domain.meal.Meal}
 * {@link com.devbugger.physicum.domain.meal.Ingredient}
 */
@Service("mealService")
@Repository
@Transactional
public class MealServiceImpl implements MealService {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    @Transactional(readOnly = true)
    public Meal findById(Long id) {
        Meal meal = mealRepository.findOne(id);
        meal.setMealNutritionValues(addMealNutritionValues(meal.getIngredients()));
        return mealRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Meal> pageableMealList(User user, int page, int pageSize) {
        List<Meal> meals = mealRepository.findMealByUserOrderByIdDesc(
                user, new PageRequest(page, pageSize));

        meals.forEach((meal) ->
                meal.setMealNutritionValues(addMealNutritionValues(meal.getIngredients())));

        return meals;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Meal> findByMealCategory(MealCategory mealCategory) {
        return Lists.newArrayList(
                mealRepository.findMealByMealCategory(mealCategory));
    }

    @Override
    public Meal save(Meal meal) {
        return mealRepository.save(meal);
    }

    @Override
    public void delete(Meal meal) {
        mealRepository.delete(meal);
    }

    /**
     * Helper method to sum {@link com.devbugger.physicum.domain.meal.MealNutritionValues} without
     * access the database. These values are only retained in memory when ever the object
     * is being requested by consumers of the service layer.
     * @param ingredients from the meal.
     * @return the MealNutritionValues object with the summarised values.
     */
    private MealNutritionValues addMealNutritionValues(Set<Ingredient> ingredients) {
        return new MealNutritionValues(ingredients);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ingredient> ingredientFindAll() {
        return Lists.newArrayList(ingredientRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public Ingredient ingredientFindById(Long id) {
        return ingredientRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ingredient> ingredientsFindByName(String name) {
        return Lists.newArrayList(ingredientRepository.findByNameStartingWith(name));
    }

    @Override
    public Ingredient ingredientSave(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }
}
