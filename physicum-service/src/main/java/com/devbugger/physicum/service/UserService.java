package com.devbugger.physicum.service;


import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.security.ValidAuthority;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 11/1/13
 */
public interface UserService {
	
	/**
	 * List all users in the system
	 * @return List<User>
	 */
	public List<User> findAll();

	/**
	 * Find a specific user by id
	 * @param id
	 * @return User
	 */
	public User findById(Long id);
	
	/**
	 * Finds a user specified by username.
	 * @param username
	 * @return User
	 */
	public User findByUserName(String username);

	/**
	 * Save a new user to the database using BCrypt with
	 * the default strenght(10) and a random salt.
	 * @param user
	 * @return Customer
	 */
	public User save(User user);

	/**
	 * Deletes a user from the database
	 * @param id
	 */
	public void delete(Long id);

    /**
     * Update the users credentials.
     * @param user
     */
    public void updateUserAuthority(User user, ValidAuthority validAuthority);

    /**
     * Save a profile picture to the user profile.
     * @param files
     * @param user
     * @return
     */
    public void saveProfilePicture(Map<String, MultipartFile> files, User user) throws IOException;
}