package com.devbugger.physicum.service;


import com.devbugger.physicum.domain.meal.MealCategory;
import com.devbugger.physicum.repository.MealCategoryRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("mealCategoryService")
@Repository
@Transactional
public class MealCategoryServiceImpl implements MealCategoryService {

    @Autowired
    private MealCategoryRepository mealCategoryRepository;

    @Override
    @Transactional(readOnly = true)
    public MealCategory findById(Long id) {
        return mealCategoryRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MealCategory> findAll() {
        return Lists.newArrayList(mealCategoryRepository.findAll());
    }
}
