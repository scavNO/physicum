package com.devbugger.physicum.service;


import com.devbugger.physicum.domain.User;

/**
 * What does this class do?
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 2/17/14
 */
public interface LoginService {

    /**
     * Registers a new user to the system.
     * @param user
     */
    public void register(User user);

    /**
     * Logs a user on
     * @param userName
     * @param password
     * @return
     */
    public User login(String userName, String password);

    /**
     * Logs a user out of the system.
     * @param user
     */
    public void logOut(User user);
}
