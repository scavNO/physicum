package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.meal.MealCategory;

import java.util.List;

public interface MealCategoryService {

    public MealCategory findById(Long id);

    public List<MealCategory> findAll();
}
