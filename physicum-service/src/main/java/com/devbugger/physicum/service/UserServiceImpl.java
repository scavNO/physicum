package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.repository.UserRepository;
import com.devbugger.physicum.security.ValidAuthority;
import com.devbugger.physicum.utils.FamilyboardThumbnail;
import com.devbugger.physicum.utils.ImageServlet;
import com.devbugger.physicum.utils.RandomUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("userService")
@Repository
@Transactional
public class UserServiceImpl implements UserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private PasswordEncoder passwordEncoder;

	@Override
    @Transactional(readOnly=true)
	public List<User> findAll() {
		return Lists.newArrayList(userRepository.findAll()); 
	}

    @Override
    @Transactional(readOnly=true)
	public User findById(Long id) {
		return userRepository.findOne(id);
	}
	
	@Override
    @Transactional(readOnly=true)
	public User findByUserName(String username) {
		return userRepository.findUserByUsername(username);
	}

    @Override
    @Transactional(readOnly=false)
	public User save(User user) {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authorityService.findByAuthority(ValidAuthority.ROLE_USER));
        user.setAuthorities(authorities);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

		return userRepository.save(user);
	}

	@Override
    @Transactional(readOnly=false)
	public void delete(Long id) {
		userRepository.delete(id);
	}

    @Override
    @Transactional(readOnly=false)
    public void updateUserAuthority(User user, ValidAuthority validAuthority) {
        Set<Authority> authorities = user.getAuthorities();
        authorities.add(authorityService.findByAuthority(validAuthority));
        user.setAuthorities(authorities);
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly=false)
    public void saveProfilePicture(Map<String, MultipartFile> files, User user) throws IOException {

        for(MultipartFile file : files.values()) {
            byte[] bytes = file.getBytes();

            String generatedFilename = RandomUtils.generateSafe()+file.getOriginalFilename();
            String location = ImageServlet.SYSTEM_ROOT_DIR+generatedFilename;

            File profilePicture = new File(location);
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(profilePicture));

            stream.write(bytes);
            stream.close();

            FamilyboardThumbnail.create(profilePicture);

            user.setProfilePicture(ImageServlet.IMAGE_SERVLET_ROOT_DIR+generatedFilename);
            userRepository.save(user);
        }
    }

}
