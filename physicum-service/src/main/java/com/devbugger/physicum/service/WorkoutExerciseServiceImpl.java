package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.devbugger.physicum.repository.WorkoutExerciseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("workoutExerciseService")
@Repository
@Transactional
public class WorkoutExerciseServiceImpl implements WorkoutExerciseService {

    @Autowired
    private WorkoutExerciseRepository workoutExerciseRepository;

    @Autowired
    private WorkoutService workoutService;

    @Override
    public List<WorkoutExercise> findAll() {
        return workoutExerciseRepository.findAll();
    }

    @Override
    public List<WorkoutExercise> findByWorkout(Workout workout) {
        return workoutExerciseRepository.findByPkWorkout(workout);
    }

    @Override
    public WorkoutExercise save(WorkoutExercise workoutExercise) {
        return workoutExerciseRepository.save(workoutExercise);
    }

    @Override
    public List<WorkoutExercise> saveAll(List<WorkoutExercise> workoutExercisesList) {
        return workoutExerciseRepository.save(workoutExercisesList);
    }

    @Override
    public void delete(WorkoutExercise workoutExercise) {
        workoutExerciseRepository.delete(workoutExercise);
    }
}