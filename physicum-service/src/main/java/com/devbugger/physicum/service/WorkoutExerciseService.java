package com.devbugger.physicum.service;


import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutExercise;

import java.util.List;

public interface WorkoutExerciseService {

    public List<WorkoutExercise> findAll();

    public List<WorkoutExercise> findByWorkout(Workout workout);

    public WorkoutExercise save(WorkoutExercise workoutExercise);

    public List<WorkoutExercise> saveAll(List<WorkoutExercise> workoutExercisesList);

    public void delete(WorkoutExercise workoutExercise);
}
