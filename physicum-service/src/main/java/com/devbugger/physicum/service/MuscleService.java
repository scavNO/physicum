package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.workout.Muscle;

import java.util.List;

public interface MuscleService {

    public Muscle findById(Long id);

    public List<Muscle> findAll();

    public Muscle save(Muscle muscle);

    public void delete(Muscle muscle);
}
