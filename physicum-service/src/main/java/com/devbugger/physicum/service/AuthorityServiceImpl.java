package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.repository.AuthorityRepository;
import com.devbugger.physicum.security.ValidAuthority;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("authorityService")
@Repository
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    @Transactional(readOnly = true)
    public Authority findById(Long id) {
        return authorityRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Authority> findAll() {
        return Lists.newArrayList(authorityRepository.findAll());
    }

    @Override
    @Transactional(readOnly=true)
    public Authority findByAuthority(ValidAuthority validAuthority) {
        return authorityRepository.findByAuthority(validAuthority.toString());
    }

}