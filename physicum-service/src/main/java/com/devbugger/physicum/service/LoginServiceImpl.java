package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * What does this class do?
 *
 * @author Dag Heradstveit
 * @version: 1
 * @since 2/17/14
 */
@Service("loginService")
@Repository
@Transactional
public class LoginServiceImpl implements LoginService {


    @Autowired
    private UserService userService;

    @Override
    public void register(User user) {
        String pwdSalt = BCrypt.gensalt(10);
        String pwdHash = BCrypt.hashpw(user.getPassword(), pwdSalt);
        user.setPassword(pwdHash);
        userService.save(user);
    }

    @Override
    public User login(String username, String password) {
        User user = this.userService.findByUserName(username);

        if(user != null) {
            String pwd = DigestUtils.shaHex(password + "{" + username + "}");
        }

        String pwdHash = user.getPassword();

        if(BCrypt.checkpw(password, pwdHash)) {
            return user;
        }

        else {
            return user;
        }


    }

    @Override
    public void logOut(User user) {
        // TODO Auto-generated method stub

    }
}
