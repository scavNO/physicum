package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.workout.Exercise;

import java.util.List;

public interface ExerciseService {

    public Exercise findById(Long id);

    public List<Exercise> findAll();

    public List<Exercise> findByName(String name);

    public Exercise save(Exercise exercise);

    public void delete(Exercise exercise);
}
