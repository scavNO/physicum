package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.domain.workout.Exercise;
import com.devbugger.physicum.repository.ExerciseRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("exerciseService")
@Repository
@Transactional
public class ExerciseServiceImpl  implements ExerciseService{

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Override
    @Transactional(readOnly = true)
    public Exercise findById(Long id) {
        return exerciseRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Exercise> findAll() {
        return Lists.newArrayList(exerciseRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Exercise> findByName(String name) {
        return Lists.newArrayList(exerciseRepository.findByNameStartingWith(name));
    }

    @Override
    public Exercise save(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }

    @Override
    public void delete(Exercise exercise) {
        exerciseRepository.delete(exercise);
    }
}
