package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutCategory;
import com.devbugger.physicum.repository.WorkoutCategoryRepository;
import com.devbugger.physicum.repository.WorkoutExerciseRepository;
import com.devbugger.physicum.repository.WorkoutRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("workoutService")
@Repository
@Transactional
public class WorkoutServiceImpl implements WorkoutService {

    @Autowired
    private WorkoutRepository workoutRepository;

    @Autowired
    private WorkoutCategoryRepository workoutCategoryRepository;

    @Autowired
    private WorkoutExerciseRepository workoutExerciseRepository;

    @Override
    @Transactional(readOnly = true)
    public Workout findById(Long id) {
        return workoutRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Workout> findAll(User user) {
        return workoutRepository.findAllByUser(user);
    }

    @Override
    public Workout save(Workout workout) {
        return workoutRepository.save(workout);
    }

    @Override
    public void delete(Workout workout) {
        workoutRepository.delete(workout);
    }

    @Override
    @Transactional(readOnly = true)
    public WorkoutCategory findCategoryById(Long id) {
        return workoutCategoryRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WorkoutCategory> findAllCategies() {
        return Lists.newArrayList(workoutCategoryRepository.findAll());
    }

    @Override
    public WorkoutCategory saveCategory(WorkoutCategory workoutCategory) {
        return workoutCategoryRepository.save(workoutCategory);
    }

    @Override
    public void deleteCategory(WorkoutCategory workoutCategory) {
        workoutCategoryRepository.delete(workoutCategory);

    }
}