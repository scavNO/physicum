package com.devbugger.physicum.service;

import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutCategory;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import com.devbugger.physicum.domain.workout.WorkoutExerciseId;

import java.util.List;

public interface WorkoutService {

    public Workout findById(Long id);

    public List<Workout> findAll(User user);

    public Workout save(Workout workout);

    public void delete(Workout workout);

    public WorkoutCategory findCategoryById(Long id);

    public List<WorkoutCategory> findAllCategies();

    public WorkoutCategory saveCategory(WorkoutCategory workoutCategory);

    public void deleteCategory(WorkoutCategory workoutCategory);

}