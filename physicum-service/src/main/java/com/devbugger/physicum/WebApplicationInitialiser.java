package com.devbugger.physicum;

import com.devbugger.physicum.configuration.*;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.config.BeanIds;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;
/**
 * Bootstrap the Spring container (context), adding configuration classes to the
 * context.
 *
 * @author Dag Heradstveit
 */
public class WebApplicationInitialiser implements WebApplicationInitializer {

    private static final String DISPATCHER_SERVLET_NAME = "physicumDispatcher";
    private static final String DISPATCHER_SERVLET_MAPPING = "/";
    private AnnotationConfigWebApplicationContext rootContext;

    /**
     * Contains all configuration classes that will be added to
     * {@link org.springframework.web.context.support.AnnotationConfigWebApplicationContext}
     */
    private static Class<?>[]  configurationClasses = new Class<?>[] {
            ComponentConfiguration.class,
            PropertyPlaceholderConfig.class,
            DatabaseConfiguration.class,
            WebMvcContextConfiguration.class,
            ViewConfiguration.class,
            WebSecurityConfiguration.class,
            RESTSecurityConfiguration.class
    };

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        registerListener(servletContext);
        registerDispatcherServlet(servletContext);
        registerHiddenHttpMethodFilter(servletContext);
        registerOpenEntityManagerInViewFilter(servletContext);
        registerSpringSecurityFilterChain(servletContext);

    }

    private void registerListener(ServletContext servletContext) {
        this.rootContext = createContext(configurationClasses);
        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.addListener(new RequestContextListener());
        servletContext.setInitParameter("defaultHtmlEscape", "true");
    }

    private void registerDispatcherServlet(final ServletContext servletContext) {
        WebApplicationContext dispatcherContext = createContext(configurationClasses);
        DispatcherServlet dispatcherServlet = new DispatcherServlet(dispatcherContext);
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(DISPATCHER_SERVLET_NAME, dispatcherServlet);
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping(DISPATCHER_SERVLET_MAPPING);
    }

    private AnnotationConfigWebApplicationContext createContext(final Class<?>... annotatedClasses) {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(annotatedClasses);
        return context;
    }

    /**
     * Transform requests to provide support for more methods.
     * @param servletContext injected {@link javax.servlet.ServletContext}
     */
    private void registerHiddenHttpMethodFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic registration;
        registration = servletContext.addFilter("hiddenHttpMethodFilter",
                HiddenHttpMethodFilter.class);

        registration.addMappingForServletNames(
                EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD),
                false, DISPATCHER_SERVLET_NAME);
    }

    /**
     * Avoid NPE from Hibernate closing stuff while loading the view.
     * @param servletContext injected {@link javax.servlet.ServletContext}
     */
    private void registerOpenEntityManagerInViewFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic registration = servletContext.addFilter("openEntityManagerInView",
                new OpenEntityManagerInViewFilter());
        registration.setAsyncSupported(true);
        registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), false,
        		"*");
        registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), false,
                "/j_spring_security_check");
    }

    /**
     * Register the Spring Security Filter Chain in the servlet and provides
     * intercept for all incoming requests outside the container.
     * @param servletContext injected {@link javax.servlet.ServletContext}
     */
    private void registerSpringSecurityFilterChain(ServletContext servletContext) {
        FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter(
                BeanIds.SPRING_SECURITY_FILTER_CHAIN, new DelegatingFilterProxy(BeanIds.SPRING_SECURITY_FILTER_CHAIN, rootContext));
        springSecurityFilterChain.addMappingForUrlPatterns(null, true, "/*");
    }
}