package com.devbugger.physicum.service;

import com.devbugger.physicum.configuration.ComponentConfiguration;
import com.devbugger.physicum.configuration.DatabaseConfiguration;
import com.devbugger.physicum.configuration.WebSecurityConfiguration;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.domain.meal.MealCategory;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {
        ComponentConfiguration.class,
        PropertyPlaceholderConfig.class,
        DatabaseConfiguration.class,
        WebSecurityConfiguration.class
})
@Transactional
public class MealServiceTest {

    @Autowired
    public MealService mealService;

    @Autowired
    public UserService userService;

    @Autowired
    public MealCategoryService mealCategoryService;

    @Test()
    public void findById() throws Exception  {
        Meal meal = mealService.findById((long) 52);
        assertNotNull(meal);
    }

    @Test()
    public void findPagination() throws Exception {
        List<Meal> mealList = mealService.pageableMealList(userService.findByUserName("admin"), 0, 4);
        mealList.forEach(TestCase::assertNotNull);
    }

    @Test()
    public void mealNutritionValuesSingleMeal() throws Exception {
        Meal meal = mealService.findById((long) 52);
        assertNotNull(meal.getMealNutritionValues());
    }

    @Test()
    public void MealNutritionValuesMultiplemeals() throws Exception {
        List<Meal> mealList = mealService.pageableMealList(userService.findByUserName("admin"), 0, 4);
        mealList.forEach((meal) -> assertNotNull(meal.getMealNutritionValues()));
    }

    @Test()
    public void MealByCategory() throws Exception {
        MealCategory mealCategory = mealCategoryService.findById((long) 4);
        List<Meal> mealList = mealService.findByMealCategory(mealCategory);

        mealList.forEach((meal) -> System.out.println(meal.getMealCategory()));
    }
}