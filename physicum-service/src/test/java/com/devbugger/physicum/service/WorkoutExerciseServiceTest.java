package com.devbugger.physicum.service;

import com.devbugger.physicum.configuration.ComponentConfiguration;
import com.devbugger.physicum.configuration.DatabaseConfiguration;
import com.devbugger.physicum.configuration.WebSecurityConfiguration;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.workout.Workout;
import com.devbugger.physicum.domain.workout.WorkoutExercise;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {
        ComponentConfiguration.class,
        PropertyPlaceholderConfig.class,
        DatabaseConfiguration.class,
        WebSecurityConfiguration.class
})
@Transactional
public class WorkoutExerciseServiceTest {

    @Autowired
    private WorkoutExerciseService workoutExerciseService;

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private UserService userService;

    @Test()
    public void findExerciseByWorkoutTest() throws Exception {
        Workout workout = workoutService.findById((long) 6);
        List<WorkoutExercise> workoutExercises = workoutExerciseService.findByWorkout(workout);

        workoutExercises.forEach((workoutExercise) -> {
            System.out.println(workoutExercise.getWorkout().getName());
            System.out.println(workoutExercise.getExercise().getName());
            System.out.println(workoutExercise.getReps());
            System.out.println(workoutExercise.getSets());
        });
    }

    @Test()
    public void saveNew() throws Exception {
        Workout workout = new Workout();

        workout.setDate(new Date());
        workout.setName("");
        workout.setNoteBefore("Testing the workout service before.");
        workout.setNoteAfter("Testing the workout service after.");
        workout.setUser(userService.findById((long) 1));
        workout.setWorkoutCategory(workoutService.findCategoryById((long) 1));

        WorkoutExercise a = new WorkoutExercise();
        a.setWorkout(workout);
        a.setExercise(exerciseService.findById((long) 1));
        a.setReps(8);
        a.setSets(4);

        WorkoutExercise b = new WorkoutExercise();
        b.setWorkout(workout);
        b.setExercise(exerciseService.findById((long) 2));
        a.setReps(8);
        a.setSets(3);

        WorkoutExercise c = new WorkoutExercise();
        c.setWorkout(workout);
        c.setExercise(exerciseService.findById((long) 4));
        a.setReps(8);
        a.setSets(3);

        workoutService.save(workout);

        workoutExerciseService.save(a);
        workoutExerciseService.save(b);
        workoutExerciseService.save(c);
    }

}