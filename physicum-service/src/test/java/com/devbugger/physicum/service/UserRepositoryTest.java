package com.devbugger.physicum.service;

import com.devbugger.physicum.configuration.ComponentConfiguration;
import com.devbugger.physicum.configuration.DatabaseConfiguration;
import com.devbugger.physicum.configuration.WebSecurityConfiguration;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.Authority;
import com.devbugger.physicum.domain.User;
import com.devbugger.physicum.repository.UserRepository;
import com.devbugger.physicum.security.ValidAuthority;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {
        ComponentConfiguration.class,
        PropertyPlaceholderConfig.class,
        DatabaseConfiguration.class,
        WebSecurityConfiguration.class
})

@Transactional
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void addUser() throws Exception {
        User user = new User();

        user.setUsername("juser");
        user.setName("unit user");
        user.setEmail("user@unit-t.com");
        user.setPassword("password");

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        Set<Authority> authorities = new HashSet<>();
        authorities.add(authorityService.findByAuthority(ValidAuthority.ROLE_USER));
        user.setAuthorities(authorities);

         userRepository.save(user);

    }
}