package com.devbugger.physicum.service;

import com.devbugger.physicum.configuration.ComponentConfiguration;
import com.devbugger.physicum.configuration.DatabaseConfiguration;
import com.devbugger.physicum.configuration.WebSecurityConfiguration;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.workout.Workout;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {
        ComponentConfiguration.class,
        PropertyPlaceholderConfig.class,
        DatabaseConfiguration.class,
        WebSecurityConfiguration.class
})
@Transactional
public class WorkoutServiceTest {

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test()
    public void addWorkout() throws Exception {
        Workout workout = new Workout();

        workout.setDate(new Date());
        workout.setName("");
        workout.setNoteBefore("Testing the workout service before.");
        workout.setNoteAfter("Testing the workout service after.");
        workout.setUser(userService.findById((long) 1));
        workout.setWorkoutCategory(workoutService.findCategoryById((long) 1));

        workoutService.save(workout);
    }

    @Test()
    public void pwd() throws Exception {
        System.out.println(bCryptPasswordEncoder.encode("admin"));
    }
}
