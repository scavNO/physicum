package com.devbugger.physicum.service;

import com.devbugger.physicum.configuration.ComponentConfiguration;
import com.devbugger.physicum.configuration.DatabaseConfiguration;
import com.devbugger.physicum.configuration.WebSecurityConfiguration;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.meal.MealCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {
        ComponentConfiguration.class,
        PropertyPlaceholderConfig.class,
        DatabaseConfiguration.class,
        WebSecurityConfiguration.class
})
@Transactional
public class MealCategoryServiceTest {

    @Autowired
    private MealCategoryService mealCategoryService;

    @Test()
    public void findAll() {
        List<MealCategory> mealCategoryList = mealCategoryService.findAll();

        mealCategoryList.forEach(System.out::println);
    }
}
