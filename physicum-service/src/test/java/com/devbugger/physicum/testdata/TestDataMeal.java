package com.devbugger.physicum.testdata;


import com.devbugger.physicum.domain.meal.Ingredient;
import com.devbugger.physicum.domain.meal.Meal;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TestDataMeal {

    public static Meal defaultMeal() {
        Meal meal = new Meal();

        Set<Ingredient> ingredientList = new HashSet<>();
        Ingredient ingredient1 = new Ingredient("Broccoli", 10, 20, 30, 40, 50, 60, 70);
        ingredient1.setId(1L);
        Ingredient ingredient2 = new Ingredient("Potatoes", 10, 20, 30, 40, 50, 60, 70);
        ingredient2.setId(2L);
        Ingredient ingredient3 = new Ingredient("Beef", 10, 20, 30, 40, 50, 60, 70);
        ingredient3.setId(2L);

        ingredientList.add(ingredient1);
        ingredientList.add(ingredient2);
        ingredientList.add(ingredient3);

        meal.setIngredients(ingredientList);

        meal.setNote("Test Meal");
        meal.setDate(new Date());

        return meal;
    }
}
