package com.devbugger.physicum.controller;

import com.devbugger.physicum.configuration.*;
import com.devbugger.physicum.configuration.support.PropertyPlaceholderConfig;
import com.devbugger.physicum.domain.meal.Meal;
import com.devbugger.physicum.service.MealService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration( classes = {
//        ComponentConfiguration.class,
//        PropertyPlaceholderConfig.class,
//        DatabaseConfiguration.class,
//        WebSecurityConfiguration.class,
//        WebMvcContextConfiguration.class
//})
@ContextConfiguration(classes = {
        TestContext.class
})
public class MealControllerTest {

    @Autowired
    private MealService mealService;

    @Test()
    public void testGetMeal() throws Exception {
        Meal meal = mealService.findById((long)52);
        System.out.println(meal.toString());
    }

    @Test()
    public void getTest() throws Exception {
        RestTemplate template = new RestTemplate();
        ResponseEntity<Meal> mealEntity = template.getForEntity(
                "http://localhost:8080/physicum/meal/52", Meal.class);

        Meal meal = mealEntity.getBody();

        assertThat(mealEntity.getStatusCode().toString(), is("200"));
        assertNotNull(meal);
    }

}
