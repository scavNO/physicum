package com.devbugger.physicum.configuration;

import com.devbugger.physicum.service.MealService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestContext {

    @Bean
    public MealService mealService() {
        return Mockito.mock(MealService.class);
    }
}
